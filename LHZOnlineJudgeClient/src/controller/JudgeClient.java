package controller;

import java.net.*;
import java.io.*;

/**
* 基于请求响应模型的客户端套接字，只有发送报文一个方法
* @author 林智凯
* @version 1.1
*/
public class JudgeClient
{
	/**
	   * 这个方法用于向服务器发送请求
	   * @param actionCode 用户的操作行为码，String
	   * @param username 用户名(非加密)，String
	   * @param classId 班级id，Integer
	   * @param collectionId 题目集id，Integer
	   * @param text 提交内容，String，选择题和判断题为逗号分隔的答案字符串，编程题为“题目id + 空格 + 代码”
	   * @return 该请求是否成功，Boolean
	   */
   public static String sendAnswer(String actionCode, String username, String classId, Integer collectionId, String text)
   {
      String serverName = "127.0.0.1";
      int port = 12001;
      try
      {
         //System.out.println("连接到主机：" + serverName + " ，端口号：" + port);
         @SuppressWarnings("resource")
		Socket client = new Socket(serverName, port);
         //System.out.println("远程主机地址：" + client.getRemoteSocketAddress());
         OutputStream outToServer = client.getOutputStream();
         DataOutputStream out = new DataOutputStream(outToServer);
         
         //明文写入操作码，跟着传入的参数
         String plainText = actionCode + " " + username + " " + classId.toString() + " " + collectionId.toString() + " " + text;
         //System.out.println(Plaintext);
         //对明文进行 base64加密
         //String ciphertext = Base64.getEncoder().encodeToString(Plaintext.getBytes("utf-8"));
         //System.out.println(ciphertext);
         
         //向套接字传输密文
         out.writeUTF(plainText);
         InputStream inFromServer = client.getInputStream();
         DataInputStream in = new DataInputStream(inFromServer);
         //System.out.println("服务器响应： " + in.readUTF());
         
         //读取响应信息，base64解密后将bypes转为string
         //String result_decode = new String(Base64.getDecoder().decode(in.readUTF()));
         String resultDecode = new String(in.readUTF());
         return resultDecode;
        
      }catch(IOException e)
      {
         e.printStackTrace();
      }
      
	return null;
   }
   
   public static void main(String [] args)
   {
	   String code = " #include<stdio.h>\r\n" + 
	   		"int main()\r\n" + 
	   		"{\r\n" + 
	   		"    printf(\"Hello World!\");\r\n" + 
	   		"    return 0;\r\n" + 
	   		"}";
	   System.out.println(JudgeClient.sendAnswer("1", "郑梦露", "C语言网络1911", 1, "2 " + code));
   }
}