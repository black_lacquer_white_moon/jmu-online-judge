package dao;

import java.util.List;

public interface TaskRepositoryDAO {
	/**
	 *  该方法用于获取某班级所有做的题集
	 * 
	 * @param classname 班级名称，String
	 * @return 题集id序列，List<Integer>
	 */
	public List<Integer> getClassTask(String classname);
	/**
	 *， 该方法给班级添加一个要做的题集
	 * 
	 * @param collectionId 题集id，Integer
	 * @param classname     班级名称，String
	 * @return 插入是否成功 boolean
	 */

	public boolean setClassTask(String classname, Integer collectionId) ;
	/**
	 * 该方法用于删除班级的一个题集
	 * @param collectionId 题集id，Integer
	 * @param classname     班级名称，String
	 * @return 删除是否成功 boolean
	 */
	public boolean deleteClassTask(String classname, int collectionId);
}
