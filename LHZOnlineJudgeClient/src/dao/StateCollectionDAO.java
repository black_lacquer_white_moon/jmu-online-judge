package dao;

import java.sql.SQLException;
import java.util.List;

/**
* StateCollectionDAO 接口获取已做和未做题目集的方法
* @author 林智凯
* @version 1.0
*/
public interface StateCollectionDAO {
	
	/**
	   * 该方法用于将从存储层查找未做题目集
	   * @param username 用户名，String
	   * @return List<Integer>：List集合，存储未做题目集的编号。
	   * @throws SQLException 数据库异常
	   */
	public List<Integer> getIncompleteCollections(String username) throws SQLException;
	
	/**
	   * 该方法用于将从存储层查找完成题目集
	   * @param username 用户名，String
	   * @return List<Integer>：List集合，存储已做题目集的编号。
	   * @throws SQLException 数据库异常
	   */
	public List<Integer> getCcompleteCollections(String username) throws SQLException;
}
