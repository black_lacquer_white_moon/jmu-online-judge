package dao;

import java.util.List;

import model.ProgrammingQuestion;
/**
* @author ZML
* @version 1.0
*/

public interface ProgrammingDAO {
	/**
	   * 该方法用于获取数据库中的编程类型题集,返回List的该对象集。
	   * @param  programmingQuestionsId 编程题号序列，List<Integer> 
	   * @return 编程题对象集 ：List<ProgrammingQuestion> 
	   */
	public  List<ProgrammingQuestion> getData(List<Integer> programmingQuestionsId);
	/**

	   * 该方法用于向数据库中插入设置的编程题信息，存储数据
	   * @param List<ProgrammingQuestion> 判断题对象集，List<ProgrammingQuestion>
	   * @return 插入是否都成功 boolean
	   */
	
	public boolean storeData(List<ProgrammingQuestion> programmingQuestions);
	/**
	   * 该方法用于向数据库中删除编程题信息。 当错误信息，重新设置时可能用到
	   * @param  programmingQuestionsId 要删除的选择题题号序列，List<Integer>
	   * @return 删除是否都成功 boolean
	   */
	public boolean deleteData(List<Integer> programmingQuestionsId) ;
}