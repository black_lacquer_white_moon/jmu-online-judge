package dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import controller.UserClient;
import dao.UserDAO;
import util.Md5Util;
import util.MysqlConnect;

/**
 * 实现UserBehaviorDAO接口，支持针对用户的行为
 * 
 * @author 林智凯
 * @version 1.0
 */
public class UserDaoImpl implements UserDAO {

	/**
	 * 这个方法用于向服务器发送用户注册的请求，允许在未登录状态调用
	 * @param username 明文形式的用户名，String
	 * @param password 密码，String
	 * @return 注册操作是否成功，Boolean
	 */
	public static boolean registerUser(String username, String password) {
		return UserClient.sendRequest("1", Md5Util.getMd5Str(username), Md5Util.getMd5Str(password));
	}

	/**
	 * 这个方法用于向服务器发送用户登录的请求，允许在未登录状态调用
	 * 
	 * @param username 明文形式的用户名，String
	 * @param password 密码，String
	 * @return 登录操作是否成功，Boolean
	 */
	public static boolean signIn(String username, String password) {
		return UserClient.sendRequest("2", Md5Util.getMd5Str(username), Md5Util.getMd5Str(password));
	}

	/**
	 * 这个方法用于向服务器发送用户改密码的请求，不允许在未登录状态调用
	 * 
	 * @param username     MD5形式的用户名，String
	 * @param password     原密码，String
	 * @param newPassword 新密码，String
	 * @return 改密码操作是否成功，Boolean
	 */
	public static boolean changePassword(String username, String password, String newPassword) {
		return UserClient.sendRequest("3", username,
				Md5Util.getMd5Str(password) + " " + Md5Util.getMd5Str(newPassword));
	}

	/**
	 * 这个方法用于向服务器注销用户注册的请求，不允许在未登录状态调用
	 * 
	 * @param username MD5形式的用户名，String
	 * @param password 密码，String
	 * @return 注销操作是否成功，Boolean
	 */
	public static boolean cancelUser(String username, String password) {
		return UserClient.sendRequest("4", username, Md5Util.getMd5Str(password));
	}
	
	/**
	   * 核对用户名对应的用户类型是教师还是学生
	   * @param username 用户名
	   * @return true为老师，false为学生
	 * @throws SQLException 数据库异常                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
	   */
	public static boolean checkType(String username) throws SQLException{
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;    
		//创建 ResultSet 结果集对象
		ResultSet rs = null;	
		boolean flag = false;
		
		try {
			//数据库连接
			conn = MysqlConnect.connectDatabase(); 
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlSelect = "SELECT type FROM users WHERE binary username = '%s';";
			//查询用户名对应的用户类型
			rs = statement.executeQuery(String.format(sqlSelect, username));
			String type = null;
			//获取用户类型
			while(rs.next())
	        {
				type = rs.getString("type");
	        }
			//System.out.println(type);
			String str = "2";
			if(str.equals(type)) {
				 //用户名为学生
				flag = false;   
			}
			else {
				//用户名为老师
			    flag = true;    
			}
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			MysqlConnect.close(conn);
		}
		
		return flag;
	}
	
	/*public static void main(String[] args) throws SQLException {
		System.out.println(UserDaoImpl.checkType("615db57aa314529aaa0fbe95b3e95bd3"));
	}*/
}
