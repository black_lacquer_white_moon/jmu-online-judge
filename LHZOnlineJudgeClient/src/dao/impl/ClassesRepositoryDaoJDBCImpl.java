package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dao.ClassesRepositoryDAO;
import util.MysqlConnect;

/**
 * 基于MySql实现的ClassesRepositoryDAO接口 ，班级管理记录的相关方法
 * 
 * @author ZML
 * @version 1.0
 */


public class ClassesRepositoryDaoJDBCImpl implements ClassesRepositoryDAO {
	/**
	 * 基于 MySql 数据库实现的storeData（） 方法。 用来存储数据的方法。通常当 同学加入班级 |老师创建班级时 调用该方法
	 * 
	 * @param username  用户名称，String
	 * @param type      用户类型，Integer ，0为学生|1为老师
	 * @param classname 班级名称，String
	 * @return boolean 插入情况是否成功
	 */
	@Override
	public boolean storeData(String username, Integer type, String classname) {
		Connection con = null; // 创建 Connection 数据库连接对象
		Statement stmt = null;
		String StrSql = "insert into classes(username,type,classname) values('%s',%d,'%s');";

		try {
			con = MysqlConnect.connectDatabase();
			stmt = con.createStatement();
			stmt.executeUpdate(String.format(StrSql, username, type, classname));

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(stmt);
			MysqlConnect.close(con);
		}
		// 插入成功返回true
		return true;
	}

	/**
	 * 基于 MySql 数据库实现的searchUnjoinedClass（） 方法 。 用于查询学生可加入的班级。
	 * 
	 * @param username 班级名称 ，String
	 * @return 返回可加入的班级名称集 List
	 */
	@Override
	public List<String> searchUnjoinedClass(String username) {
		List<String> classesName = new ArrayList<>();// 存储学生可以加入的班级名称集
		ClassesRepositoryDAO c = new ClassesRepositoryDaoJDBCImpl();
		List<String> joinedClass = c.searchByUsername(username);// 存储该学生已经加入过的班级
		Connection con = null; // 创建 Connection 数据库连接对象
		PreparedStatement pStatement = null;
		ResultSet rs = null; // 创建 ResultSet 结果集对象
		String strSql = "select*from classes where type = 1;";

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			rs = pStatement.executeQuery();
			while (rs.next()) {// 此时获取所有已创建的的班级
				if (joinedClass.contains(rs.getString("classname"))) {
					;// 该学生已加入该班级,不能重复加入
				} else {
					classesName.add(rs.getString("classname"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 返回查到的可以加入的班级名称集
		return classesName;
	}

	/**
	 * 基于 MySql 数据库实现的searchByUsername（） 方法。 通常用于查询 老师创建过哪些班级？ 同学加入过哪些班级？
	 * 
	 * @param username 用户名称，String
	 * @return 班级名称集 List
	 */
	@Override
	public List<String> searchByUsername(String username) {
		List<String> classesName = new ArrayList<>();// 存储班级名称集
		Connection con = null; // 创建 Connection 数据库连接对象
		PreparedStatement pStatement = null;
		ResultSet rs = null; // 创建 ResultSet 结果集对象
		String strSql = "select*from classes where username = ?;";

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			pStatement.setString(1, username);
			rs = pStatement.executeQuery();
			while (rs.next()) {
				classesName.add(rs.getString("classname"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 返回班级名称集
		return classesName;
	}

	/**
	 * 基于 MySql 数据库实现的searchByClassname（） 方法。 查询 班级是否存在？老师不能创建已存在的班级| 学生不能加入不存在的班级
	 * 时调用
	 * 
	 * @param classname 班级名称，String
	 * @return boolean 返回是否存在
	 */
	@Override
	public boolean searchByClassname(String classname) {
		Connection con = null; // 创建 Connection 数据库连接对象
		boolean flag = false;
		PreparedStatement pStatement = null;
		ResultSet rs = null; // 创建 ResultSet 结果集对象
		String strSql = "select 1 from classes where classname= ?;";

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			pStatement.setString(1, classname);
			rs = pStatement.executeQuery();
			while (rs.next()) {// 有结果则说明存在
				flag = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 返回查询的结果集
		return flag;
	}

	/**
	 * 基于 MySql 数据库实现的 deleteClass（） 方法。 删除某班某同学，删除的是单条记录
	 * 
	 * @param classname 班级名称，String
	 * @param username  班级名称，String
	 * @return boolean 返回删除情况是否完全成功
	 */
	@Override
	public boolean deleteStudent(String username, String classname) {
		Connection con = null; // 创建 Connection 数据库连接对象
		PreparedStatement pStatement = null;
		String strSql = "delete from classes where username = ? and classname = ?;";

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			pStatement.setString(1, username);
			pStatement.setString(2, classname);

			if (pStatement.executeUpdate() == 0) {
				return false;// 删除失败返回false
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 删除成功返回true
		return true;
	}

	/**
	 * 基于 MySql 数据库实现的 deleteClass（） 方法。 用于删除整个班级 ，删除关于该班级的相关记录，是批量删除
	 * 
	 * @param classname 班级名称，String
	 * @return boolean 返回删除情况是否完全成功
	 */
	@Override
	public boolean deleteClass(String classname) {
		Connection con = null; // 创建 Connection 数据库连接对象
		PreparedStatement pStatement = null;
		String strSql = "delete from classes where classname = ?;";

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			pStatement.setString(1, classname);
			if (pStatement.executeUpdate() == 0) {
				return false;// 删除失败返回false
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 删除成功返回true
		return true;
	}

}
