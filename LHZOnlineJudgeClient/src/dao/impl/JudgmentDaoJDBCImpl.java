package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.JudgmentDAO;
import model.JudgmentQuestion;
import util.MysqlConnect;

/**
 * 基于 MySQL 实现的JudgementDAO接口
 * 
 * @author ZML
 * @version 1.0
 */

public class JudgmentDaoJDBCImpl implements JudgmentDAO {
	/**
	 * 基于 MySql 数据库实现的getData（） 方法， 该方法用于获取数据库中的判断题类型题集,返回List的该对象集。
	 * 
	 * @param judgmentQuestionsId 判断题题号序列，List<Integer>
	 * @return 判断题对象集 ：List<JudgementQuestion>List<JudgmentQuestion>
	 */
	@Override
	public List<JudgmentQuestion> getData(List<Integer> judgmentQuestionsId) { // 获取选择题对象
		List<JudgmentQuestion> judgementQuestions = new ArrayList<>();// 要返回的对象集
		Connection con = null; // 创建 Connection 数据库连接对象
		ResultSet rs = null; // 创建 ResultSet 结果集对象
		PreparedStatement pStatement = null;// 预编译的SQL语句对象
		String strSql = "select*from judgment where Id=?;";

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			// 在表中根据判断题题号依次查找
			for (Integer i : judgmentQuestionsId) {
				pStatement.setInt(1, i);// 设置要查找的题号
				rs = pStatement.executeQuery();

				while (rs.next()) {
					// 获取该题目对象
					JudgmentQuestion j = new JudgmentQuestion();
					j.setId(rs.getInt("id"));
					j.setText(rs.getString("text"));
					j.setAnswer(rs.getString("answer"));

					// 存储进判断题对象列表
					judgementQuestions.add(j);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(rs);
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 返回获取的判断题对象列表
		return judgementQuestions;
	}

	/**
	 * 基于 MySql 数据库实现的storeData（） 方法， 该方法用于向数据库中插入设置的判断题信息，存储数据
	 * 
	 * @param JudgmentQuestions 判断题对象集，List<JudgmentQuestion>
	 * @return 插入是否都成功 boolean
	 */
	@Override
	public boolean storeData(List<JudgmentQuestion> judgmentQuestions) {
		Connection con = null; // 创建 Connection 数据库连接对象
		PreparedStatement pStatement = null;
		String strSql = "insert into judgment(text,answer) values(?,?)";

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			// 遍历所有判断题对象
			for (JudgmentQuestion c : judgmentQuestions) {

				// 设置SQL语句参数
				pStatement.setString(1, c.getText());
				pStatement.setString(2, c.getAnswer());

				pStatement.addBatch();// 将当前参数加入对象列表
			}
			int[] num = pStatement.executeBatch();// 将一批命令提交给数据库执行
			for (int i : num) {
				if (i == 0) { // 若有出现插入失败的题目则返回false
					return false;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 全部插入成功返回true
		return true;
	}

	/**
	 * 基于 MySql 数据库实现的deleteData（） 方法， 该方法用于向数据库中删除判断题信息。 当错误信息，重新设置时可能用到
	 * 
	 * @param judgmentQuestionsId 要删除的选择题题号序列，List<Integer>
	 * @return 删除是否都成功 boolean
	 */
	@Override
	public boolean deleteData(List<Integer> judgmentQuestionsId) {
		int[] num;
		Connection con = null; // 创建 Connection 数据库连接对象
		PreparedStatement pStatement = null;// 预编译的SQL语句对象
		String strSql = "delete from judgment where Id=?;";

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			// 在表中根据选择题题号依次查找
			for (Integer i : judgmentQuestionsId) {
				pStatement.setInt(1, i);// 设置要查找的题号
				pStatement.addBatch();
			}
			num = pStatement.executeBatch();
			for (int i : num) {
				if (i == 0) {
					return false;
				}
			}
		} catch (

		SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		return true;
	}
}
