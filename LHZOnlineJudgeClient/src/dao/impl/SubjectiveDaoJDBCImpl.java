package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.SubjectiveDAO;
import model.SubjectiveQuestion;
import util.MysqlConnect;

/**
 * 基于 MySQL 实现的SubjectiveDAO接口
 * 
 * @author ZML
 * @version 1.0
 */

public class SubjectiveDaoJDBCImpl implements SubjectiveDAO {
	/**
	 * 基于 MySql 数据库实现的getData（） 方法， 该方法用于获取数据库中的主观类型题集,返回List的该对象集。
	 * 
	 * @param subjectiveQuestionsId 主观题题号序列，List<Integer>
	 * @return 主观题对象集 ：List<SubjectiveQuestion>
	 */
	@Override
	public List<SubjectiveQuestion> getData(List<Integer> subjectiveQuestionsId) { // 获取选择题对象
		List<SubjectiveQuestion> subjectiveQuestions = new ArrayList<>();// 要返回的对象集
		Connection con = null; // 创建 Connection 数据库连接对象
		ResultSet rs = null; // 创建 ResultSet 结果集对象
		PreparedStatement pStatement = null;// 预编译的SQL语句对象
		String strSql = "select*from subjective where Id=?;";

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			// 在表中根据主观题号依次查找
			for (Integer i : subjectiveQuestionsId) {
				pStatement.setInt(1, i);// 设置要查找的题号
				rs = pStatement.executeQuery();

				while (rs.next()) {
					// 获取该题目对象
					SubjectiveQuestion s = new SubjectiveQuestion();
					s.setId(rs.getInt("id"));
					s.setText(rs.getString("text"));
					s.setScore(rs.getInt("score"));

					// 存储进主观题对象列表
					subjectiveQuestions.add(s);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(rs);
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 返回获取的主观对象列表
		return subjectiveQuestions;
	}

	/**
	 * 基于 MySql 数据库实现的storeData（） 方法， 该方法用于向数据库中插入设置的主观题信息，存储数据
	 * 
	 * @param subjectiveQuestions 判断题对象集，List<SubjectiveQuestion>
	 * @return 插入是否都成功 boolean
	 */
	@Override
	public boolean storeData(List<SubjectiveQuestion> subjectiveQuestions) {
		Connection con = null; // 创建 Connection 数据库连接对象
		PreparedStatement pStatement = null;
		String strSql = "insert into subjective(text,score) values(?,?);";

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			// 遍历所有主观题对象
			for (SubjectiveQuestion s : subjectiveQuestions) {

				// 设置SQL语句参数

				pStatement.setString(1, s.getText());
				pStatement.setInt(2, s.getScore());

				pStatement.addBatch();// 将当前参数加入对象列表
			}
			int[] num = pStatement.executeBatch();// 将一批命令提交给数据库执行
			for (int i : num) {
				if (i == 0) { // 若有出现插入失败的题目则返回false
					return false;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 全部插入成功返回true
		return true;
	}

	/**
	 * 基于 MySql 数据库实现的deleteData（） 方法， 该方法用于向数据库中删除主观题信息。 当错误信息，重新设置时可能用到
	 * 
	 * @param subjectiveQuestionsId 要删除的选择题题号序列，List<Integer>
	 * @return 删除是否都成功 boolean
	 */
	@Override
	public boolean deleteData(List<Integer> subjectiveQuestionsId) {
		int[] num;
		Connection con = null; // 创建 Connection 数据库连接对象
		PreparedStatement pStatement = null;// 预编译的SQL语句对象
		String strSql = "delete from subjective where Id=?;";

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			// 在表中根据选择题题号依次查找
			for (Integer i : subjectiveQuestionsId) {
				pStatement.setInt(1, i);// 设置要查找的题号
				pStatement.addBatch();
			}
			num = pStatement.executeBatch();
			for (int i : num) {
				if (i == 0) {
					return false;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		return true;
	}
}
