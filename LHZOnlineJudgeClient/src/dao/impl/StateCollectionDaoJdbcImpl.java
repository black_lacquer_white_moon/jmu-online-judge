package dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import dao.StateCollectionDAO;
import util.MysqlConnect;

/**
* StateCollectionDAO 接口基于MySQL数据库的实现类
* @author 林智凯
* @version 1.0
*/
public class StateCollectionDaoJdbcImpl implements StateCollectionDAO {

	@Override
	/**
	   * 该方法用于将从存储层查找未做题目集
	   * @param username 用户名，String
	   * @return List<Integer>：List集合，存储未做题目集的编号。
	   */
	public List<Integer> getIncompleteCollections(String username) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;    
		//创建 ResultSet 结果集对象
		ResultSet rs = null;	
		
		try {
			//数据库连接
			conn = MysqlConnect.connectDatabase();    
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlSelect = "SELECT collection_id FROM score WHERE binary username = '%s' AND "
					+ "(choice = -1 OR judgment = -1 OR programming = -1 OR subjective = -1);";
			rs = statement.executeQuery(String.format(sqlSelect, username));
			//将结果集的数据存到LinkedList
			List<Integer> collectionIds = new LinkedList<Integer>();
			while(rs.next())
	        {
				Integer id = Integer.valueOf(rs.getInt("collection_id"));
				collectionIds.add(id);
	        }
			return collectionIds;
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    //关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			MysqlConnect.close(conn);
		}
	}

	@Override
	/**
	   * 该方法用于将从存储层查找完成题目集
	   * @param username 用户名，String
	   * @return List<Integer>：List集合，存储已做题目集的编号。
	   */
	public List<Integer> getCcompleteCollections(String username) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;    
		//创建 ResultSet 结果集对象
		ResultSet rs = null;	
		
		try {
			//数据库连接
			conn = MysqlConnect.connectDatabase();    
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlSelect = "SELECT collection_id FROM score WHERE binary username = '%s' AND "
					+ "(choice >= 0 AND judgment >= 0 AND programming >= 0 AND subjective >= 0);";
			rs = statement.executeQuery(String.format(sqlSelect, username));
			//将结果集的数据存到LinkedList
			List<Integer> collectionIds = new LinkedList<Integer>();
			while(rs.next())
	        {
				Integer id = Integer.valueOf(rs.getInt("collection_id"));
				collectionIds.add(id);
	        }
			return collectionIds;
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			MysqlConnect.close(conn);
		}
	}

	/*public static void main(String [] args) throws SQLException
	{
		StateCollectionDAOJDBCImpl text = new StateCollectionDAOJDBCImpl();
		System.out.println(text.getIncompleteCollections("吴海波"));
	}*/
}
