package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dao.ChoiceDAO;
import dao.ClassesRepositoryDAO;
import model.ChoiceQuestion;
import util.MysqlConnect;

/**
* 基于 MySQL 实现的 ChoiceDAO接口
* @author ZML
* @version 1.0
*/

public class ChoiceDaoJDBCImpl implements ChoiceDAO{
	/**
	   * 基于 MySql 数据库实现的getData（） 方法，
	   * 该方法用于获取数据库中的选择题类型题集,返回List的该对象集。
	   * @param choiceQuestionsId 选择题题号序列，List<Integer> 
	   * @return 选择题对象集 ：List<ChoiceQuestion> 
	   */
	@Override
	public List<ChoiceQuestion> getData(List<Integer> choiceQuestionsId) {	// 获取选择题对象
		List<ChoiceQuestion> choiceQuestions = new ArrayList<>();//要返回的对象集
		Connection con = null;    //创建 Connection 数据库连接对象
		ResultSet rs = null; // 创建 ResultSet 结果集对象
		PreparedStatement pStatement=null;//预编译的SQL语句对象
		String strSql = "select*from choice where Id=?;"; 

		try {
			con = MysqlConnect.connectDatabase();  
			pStatement = con.prepareStatement(strSql);
			// 在表中根据选择题题号依次查找
			for (Integer i : choiceQuestionsId) {
				pStatement.setInt(1, i);//设置要查找的题号
				rs = pStatement.executeQuery();
				
				while (rs.next()) {
					// 获取该题目对象
					ChoiceQuestion q = new ChoiceQuestion();
					q.setId(rs.getInt("id"));
					q.setText(rs.getString("text"));
					q.setAnswer(rs.getString("answer"));
					//处理选项字符串
					if(rs.getString("options")!=null) {
					String[] s=rs.getString("options").split("!");//!号分隔
					List<String>options=Arrays.asList(s);
					q.setOptions(options);
					}else {
						q.setOptions(null);
					}
					// 存储进选择题对象列表
					choiceQuestions.add(q);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(rs);
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 返回获取的选择题对象列表
		return choiceQuestions;
	}

	/**
	   * 基于 MySql 数据库实现的storeData（） 方法，
	   * 该方法用于向数据库中插入设置的选择题信息，存储数据
	   * @param choiceQuestions 选择题对象集，List<ChoiceQuestion>
	   * @return 插入是否都成功 boolean
	   */
	@Override
	public boolean storeData(List<ChoiceQuestion> choiceQuestions) {
		Connection con = null;    //创建 Connection 数据库连接对象
		PreparedStatement pStatement=null;
		String strSql = "insert into choice(text,options,answer) values(?,?,?)"; 

		try {
			con = MysqlConnect.connectDatabase();  
			pStatement = con.prepareStatement(strSql);
			// 遍历所有选择题对象
			for (ChoiceQuestion c : choiceQuestions) {
				//处理选项整合成字符串
				String optionStr=c.getOptions().get(0);
				for (int j = 1; j < c.getOptions().size(); j++) {
					optionStr = optionStr+"!"+(c.getOptions().get(j));//！号分隔
				}
				//设置SQL语句参数
				pStatement.setString(1, c.getText());
				pStatement.setString(2,optionStr);
				pStatement.setString(3, c.getAnswer());
                
				pStatement.addBatch();//将当前参数加入对象列表
			}
			int[] num=pStatement.executeBatch();//将一批命令提交给数据库执行
			for (int i : num) {
				if(i==0) { //若有出现插入失败的题目则返回false
				    return false;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 返回获取的选择题对象列表
		return true;
	}
	
	/**
	   * 基于 MySql 数据库实现的deleteData（） 方法，
	   * 该方法用于向数据库中删除选择题信息。 当错误信息，重新设置时可能用到
	   * @param choiceQuestionsId 要删除的选择题题号序列，List<Integer>
	   * @return 删除是否都成功 boolean
	   */
	@Override
	public  boolean deleteData(List<Integer> choiceQuestionsId) {
        int[] num;
		Connection con = null;    //创建 Connection 数据库连接对象
		PreparedStatement pStatement=null;//预编译的SQL语句对象
		String strSql = "delete from choice where Id=?;"; 

		try {
			con = MysqlConnect.connectDatabase();  
			pStatement = con.prepareStatement(strSql);
			// 在表中根据选择题题号依次查找
			for (Integer i : choiceQuestionsId) {
				pStatement.setInt(1, i);//设置要查找的题号
				pStatement.addBatch();
			}
			num = pStatement.executeBatch();
			for (int i : num) {
				if(i==0) {
					return false;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		return true;
	}


}
