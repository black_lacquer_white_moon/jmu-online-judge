package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.TaskRepositoryDAO;
import model.SubjectiveQuestion;
import util.MysqlConnect;

public class TaskRepositoryDaoJDBCImpl implements TaskRepositoryDAO {
    /**
     * 基于 MySql 数据库实现的getClassTask（） 方法， 该方法用于获取某班级所有做的题集
     *
     * @param classname 班级名称，String
     * @return 题集id序列，List<Integer>
     */
    @Override
    public List<Integer> getClassTask(String classname) { // 获取选择题对象
        Connection con = null; // 创建 Connection 数据库连接对象
        ResultSet rs = null; // 创建 ResultSet 结果集对象
        PreparedStatement pStatement = null;// 预编译的SQL语句对象
        String strSql = "select*from task where classname =?;";

        List<Integer> collectionsList = new ArrayList<>();

        try {
            con = MysqlConnect.connectDatabase();
            pStatement = con.prepareStatement(strSql);
            pStatement.setString(1, classname);// 设置要查找班级
            rs = pStatement.executeQuery();

            while (rs.next()) {// 存储题集ID List
                collectionsList.add(rs.getInt("collection_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MysqlConnect.close(rs);
            MysqlConnect.close(pStatement);
            MysqlConnect.close(con);
        }
        // 返回获取的选择题对象列表
        return collectionsList;
    }

    /**
     * 基于 MySql 数据库实现的 setClassTask（） 方法， 该方法给班级添加一个要做的题集
     *
     * @param collectionId 题集id，Integer
     * @param classname    班级名称，String
     * @return 插入是否成功 boolean
     */
    @Override
    public boolean setClassTask(String classname, Integer collectionId) {
        Connection con = null; // 创建 Connection 数据库连接对象
        PreparedStatement pStatement = null;
        String strSql = "insert into task(classname,collection_id) values(?,?);";

        try {
            con = MysqlConnect.connectDatabase();
            pStatement = con.prepareStatement(strSql); // 设置SQL语句参数
            pStatement.setString(1, classname);
            pStatement.setInt(2, collectionId);
            if (pStatement.executeUpdate() == 0) {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MysqlConnect.close(pStatement);
            MysqlConnect.close(con);
        }
        // 全部插入返回true
        return true;
    }

    /**
     * 基于 MySql 数据库实现的deleteClassTask（） 方法。删除班级的一个题集
     *
     * @param collectionId 题集id，Integer
     * @param classname    班级名称，String
     * @return 删除是否成功 boolean
     */
    @Override
    public boolean deleteClassTask(String classname, int collectionId) {
        Connection con = null; // 创建 Connection 数据库连接对象
        PreparedStatement pStatement = null;// 预编译的SQL语句对象
        String strSql = "delete from task where classname = ? and collection_id = ?;";

        try {
            con = MysqlConnect.connectDatabase();
            pStatement = con.prepareStatement(strSql);
            pStatement.setString(1, classname);
            pStatement.setInt(2, collectionId);// 设置要查找的题号
            if (pStatement.executeUpdate() == 0) {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MysqlConnect.close(pStatement);
            MysqlConnect.close(con);
        }
        return true;
    }
}