package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.ScoreDAO;
import model.Score;
import util.MysqlConnect;

/**
 * 基于 MySQL 实现的ScoreDAO接口
 * 
 * @author ZML
 * @version 1.0
 */

public class ScoreDaoJDBCImpl implements ScoreDAO {
	/**
	 * 基于 MySql 数据库实现的getClassAllScore（） 方法， 该方法用于老师获取某班级某题集的所有同学的超级
	 * 
	 * @param classId      班级id名称,String
	 * @param collectionId 题集id,Integer
	 * @return 题集id序列，List<Integer>
	 */
	@Override
	public List<Score> getClassAllScore(String classId, Integer collectionId) { // 获取选择题对象
		Connection con = null; // 创建 Connection 数据库连接对象
		ResultSet rs = null; // 创建 ResultSet 结果集对象
		PreparedStatement pStatement = null;// 预编译的SQL语句对象
		String strSql = "select*from score where class_id =? and collection_id =?;";

		List<Score> scoreList = new ArrayList<>();

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			pStatement.setString(1, classId);// 设置要查找班级
			pStatement.setInt(2, collectionId);// 设置要查找的题集
			rs = pStatement.executeQuery();

			while (rs.next()) {// 存储成绩集
				Score s = new Score(rs.getString("username"), rs.getInt("collection_id"), rs.getInt("choice"),
						rs.getInt("judgment"), rs.getInt("programming"), rs.getInt("subjective"),
						rs.getString("class_id"));
				scoreList.add(s);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(rs);
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 返回成绩集
		return scoreList;
	}

	/**
	 * 基于 MySql 数据库实现的getClassAllScore（） 方法， 该方法用于学生获取自己已做题集的所有成绩
	 * 
	 * @param username      学生名字，String
	 * @param collectionId 题集id名称,Integer。主要应传入的是所有
	 * @return 题集id序列，List<Integer>
	 */
	@Override
	public List<Score> getDoneCollectionScore(String username, List<Integer> collectionId) { // 获取选择题对象
		Connection con = null; // 创建 Connection 数据库连接对象
		ResultSet rs = null; // 创建 ResultSet 结果集对象
		PreparedStatement pStatement = null;// 预编译的SQL语句对象
		String strSql = "select*from score where username = ? and collection_id =?;";

		List<Score> scoreList = new ArrayList<>();
		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);

			for (Integer i : collectionId) {

				pStatement.setString(1, username);// 设置要查找的同学名字
				pStatement.setInt(2, i);// 设置要查找的题集id
				rs = pStatement.executeQuery();
				if (rs.next()) {// 存储分数集
					Score s = new Score(rs.getString("username"), rs.getInt("collection_id"), rs.getInt("choice"),
							rs.getInt("judgment"), rs.getInt("programming"), rs.getInt("subjective"),rs.getString("class_id"));
					scoreList.add(s);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(rs);
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 返回成绩集
		return scoreList;
	}

	/**
	 * 基于 MySql 数据库实现的storeData（） 方法。可批量添加成绩记录
	 * 
	 * @param List<Score> scoreList
	 * @return 插入是否都成功 boolean
	 */
	@Override
	public boolean storeData(List<Score> scoreList) {
		Connection con = null; // 创建 Connection 数据库连接对象
		PreparedStatement pStatement = null;
		String strSql = "insert into score(username,collection_id,choice,judgment,programming,subjective,class_id) values(?,?,?,?,?,?,?);";
		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql); // 设置SQL语句参数

			for (Score s : scoreList) {
				pStatement.setString(1, s.getUsername());
				pStatement.setInt(2, s.getCollectionId());
				pStatement.setInt(3, s.getChoice());
				pStatement.setInt(4, s.getJudgment());
				pStatement.setInt(5, s.getProgramming());
				pStatement.setInt(6, s.getSubjective());
				pStatement.setString(7, s.getClassId());
				pStatement.addBatch();
			}
			int[] num = pStatement.executeBatch();

			for (int i : num) {
				if (i == 0) {
					return false;// 出现插入失败
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 全部插入成功返回true
		return true;
	}


}
