package dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import dao.SubmitDAO;
import controller.JudgeClient;
import util.MysqlConnect;

/**
* SubmitDAO 接口基于MySQL和Socket的实现类
* @author 林智凯
* @version 1.0
*/
public class SubmitDaoSocketImpl implements SubmitDAO{

	@Override
	/**
	   * 该方法用于提交一道主观题
	   * @param username 用户名(非加密)，String
	   * @param questiond_id 主观题题号，Integer
	   * @param text 主观题作答，string
	   * @return boolean：提交成功true，反之为false。
	   */
	public boolean submitSubjective(String username, Integer questiondId, String text) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;
		boolean flag = false;
		
		try {
			//数据库连接
			conn = MysqlConnect.connectDatabase();
			//初始化静态 SQL语句
			statement = conn.createStatement();
			String sqlInsert = " INSERT INTO answer(username, questiond_id, text) values('%s',%d,'%s'); ";
			//判断插入是否成功
			if(statement.executeUpdate(String.format(sqlInsert, username, questiondId, text)) != 0) {
				flag = true;
			}
			else {
				flag = false;
			}
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    //关闭所有资源
			MysqlConnect.close(statement);
			MysqlConnect.close(conn);
		}
		
		return flag;
	}

	@Override
	/**
	   * 该方法用于提交一个题集的选择题
	   * @param username 用户名(非加密)，String
	   * @param class_id 班级id，Integer
	   * @param collection_id 题目集id，Integer
	   * @param text 提交内容，String，选择题为逗号分隔的答案字符串
	   * @return boolean：提交成功true，反之为false。
	   */
	public String submitChoice(String username, String classId, Integer collectionId, String text) {
		// TODO Auto-generated method stub
		return JudgeClient.sendAnswer("2", username, classId, collectionId, text);
	}

	@Override
	/**
	   * 该方法用于提交一个题集的判断题
	   * @param username 用户名(非加密)，String
	   * @param class_id 班级id，Integer
	   * @param collection_id 题目集id，Integer
	   * @param text 提交内容，String，选择题为逗号分隔的答案字符串
	   * @return boolean：提交成功true，反之为false。
	   */
	public String submitJudgment(String username, String classId, Integer collectionId, String text) {
		// TODO Auto-generated method stub
		return JudgeClient.sendAnswer("3", username, classId, collectionId, text);
	}

	@Override
	/**
	   * 该方法用于提交一道编程题
	   * @param username 用户名(非加密)，String
	   * @param class_id 班级id，Integer
	   * @param collection_id 题目集id，Integer
	   * @param question_id 编程题id，Integer
	   * @param code 提交的代码，String
	   * @return boolean：提交成功true，反之为false。
	   */
	public String submitProgram(String username, String classId, Integer collectionId, Integer questionId,
			String code) {
		// TODO Auto-generated method stub
		return JudgeClient.sendAnswer("1", username, classId, collectionId, questionId.toString() + " " + code);
	}
	
}
