package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import java.util.List;

import dao.ExerciseCollectionsDAO;
import model.ExerciseCollections;
import util.MysqlConnect;

/**
 * 基于MySql实现的ExerciseCollectionsDAO接口 ，题目集与数据库相交互的方法
 * 
 * @author ZML
 * @version 1.1
 */


public class ExerciseCollectionsDaoJDBCImpl implements ExerciseCollectionsDAO {
	@Override
	/**
	 * 基于 MySql 数据库实现的GetData（） 方法。 用来获取习题集内所有题目Id的方法。可以通过题集的Id|题集的名称来进行搜索获取
	 * 
	 * @param EC 题目集对象，ExerciseCollections
	 * @return boolean 获取题集Id数据情况是否成功
	 */

	public boolean getData(ExerciseCollections exerciseCollection) {
		// 创建 Connection 数据库连接对象
		Connection con = null;
		// 创建 ResultSet 结果集对象
		ResultSet rs = null;
		// 预编译的SQL语句对象
		PreparedStatement pStatement = null;

		String strSql1 = "select*from collections where Id = ?";
		String strSql2 = "select*from collections where name = ?";

		try {
			con = MysqlConnect.connectDatabase();

			if (exerciseCollection.getId() != null) {// 根据ID查
				pStatement = con.prepareStatement(strSql1);
				pStatement.setInt(1, exerciseCollection.getId());
			} else {// 根据姓名查
				pStatement = con.prepareStatement(strSql2);
				pStatement.setString(1, exerciseCollection.getName());
			}

			rs = pStatement.executeQuery();
			if (rs == null) {// 未查到与该题集相关的信息 ，返回false失败
				return false;
			}

			while (rs.next()) {
				// 获取完整属性
				exerciseCollection.setId(rs.getInt("Id"));
				exerciseCollection.setName(rs.getString("name"));
				exerciseCollection.setScoreChoice(rs.getInt("score_choice"));
				exerciseCollection.setScoreJudgment(rs.getInt("score_judgment"));

				// 获取选择题题号序列List
				if (rs.getString("choice") == null)
					exerciseCollection.setChoiceQuestionsId(null);
				else {
					// 分隔字符串
					String[] choice = rs.getString("choice").split(",");
					// 转化为Integer序列
					Integer[] choiceId = new Integer[choice.length];
					choiceId[0] = Integer.parseInt(choice[0]);
					for (int i = 1; i < choice.length; i++) {
						choiceId[i] = Integer.parseInt(choice[i]);
					}
					// 从数组转化为List
					List<Integer> choiceQuestionsId = Arrays.asList(choiceId);
					// 存储进属性
					exerciseCollection.setChoiceQuestionsId(choiceQuestionsId);
				}

				if (rs.getString("judgment") == null)
					exerciseCollection.setJudgmentQuestionsId(null);
				else {
					String[] judgment = rs.getString("judgment").split(",");
					Integer[] judgmentId = new Integer[judgment.length];
					for (int i = 0; i < judgment.length; i++) {
						judgmentId[i] = Integer.parseInt(judgment[i]);
					}
					List<Integer> judgmentQuestionsId = Arrays.asList(judgmentId);
					exerciseCollection.setJudgmentQuestionsId(judgmentQuestionsId);
				}

				if (rs.getString("subjective") == null)
					exerciseCollection.setSubjectiveQuestionsId(null);
				else {
					String[] subjective = rs.getString("subjective").split(",");
					Integer[] subjectiveId = new Integer[subjective.length];
					for (int i = 0; i < subjective.length; i++) {
						subjectiveId[i] = Integer.parseInt(subjective[i]);
					}
					List<Integer> subjectiveQuestionsId = Arrays.asList(subjectiveId);
					exerciseCollection.setSubjectiveQuestionsId(subjectiveQuestionsId);
				}

				if (rs.getString("programming") == null)
					exerciseCollection.setProgrammingQuestionsId(null);
				else {
					String[] programming = rs.getString("programming").split(",");
					Integer[] programmingId = new Integer[programming.length];
					for (int i = 0; i < programming.length; i++) {
						programmingId[i] = Integer.parseInt(programming[i]);
					}
					List<Integer> programmingQuestionsId = Arrays.asList(programmingId);
					exerciseCollection.setProgrammingQuestionsId(programmingQuestionsId);

					// rs有结果说明查找成功
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(rs);
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		} // 查找失败
		return false;
	}

	@Override
	/**
	 * 基于 MySql 数据库实现的getExercise（） 方法。 根据题目Id,调用相关类型题目方法，获得题目对象集
	 *
	 * @param EC 题目集对象，ExerciseCollections
	 * @return boolean 获取题集数据情况是否成功
	 */

	public void getExercise(ExerciseCollections exerciseCollection) {
		exerciseCollection
				.setChoiceQuestions((new ChoiceDaoJDBCImpl()).getData(exerciseCollection.getChoiceQuestionsId()));
		exerciseCollection
				.setJudgmentQuestions((new JudgmentDaoJDBCImpl()).getData(exerciseCollection.getJudgmentQuestionsId()));
		exerciseCollection.setSubjectiveQuestions(
				(new SubjectiveDaoJDBCImpl()).getData(exerciseCollection.getSubjectiveQuestionsId()));
		exerciseCollection.setProgrammingQuestions(
				(new ProgrammingDaoJDBCImpl()).getData(exerciseCollection.getProgrammingQuestionsId()));
	}

	@Override
	/**
	 * 基于 MySql 数据库实现的storeData（） 方法。存储题集的Id集，即数据库中存储题集有什么题目
	 * 
	 * @param EC 题目集对象，ExerciseCollections
	 * @return boolean 插入记录是否成功
	 */
	public boolean storeData(ExerciseCollections exerciseCollection) {
		Connection con = null; // 创建 Connection 数据库连接对象
		PreparedStatement pStatement = null;
		String strSql = "insert into collections(name,score_choice,score_judgment,choice,judgment,subjective,programming) values(?,?,?,?,?,?,?);";

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			// 若有id序列则处理选项整合成字符串并给参数赋值，否则赋为null
			if (exerciseCollection.getChoiceQuestionsId().size() != 0) {
				String choiceStr = null;
				for (int j = 0; j < exerciseCollection.getChoiceQuestionsId().size(); j++) {
					choiceStr = exerciseCollection.getChoiceQuestionsId().get(j) + ",";// ,号分隔
				}

				pStatement.setString(4, choiceStr);
			} else {
				pStatement.setString(4, null);
			}
			if (exerciseCollection.getJudgmentQuestionsId().size() != 0) {
				String judgmentStr = null;
				for (int j = 0; j < exerciseCollection.getJudgmentQuestionsId().size(); j++) {
					judgmentStr = exerciseCollection.getJudgmentQuestionsId().get(j) + ",";// ,号分隔
				}
				pStatement.setString(5, judgmentStr);
			} else {
				pStatement.setString(5, null);
			}

			if (exerciseCollection.getSubjectiveQuestionsId().size() != 0) {
				String subjectiveStr = null;
				for (int j = 0; j < exerciseCollection.getSubjectiveQuestionsId().size(); j++) {
					subjectiveStr = exerciseCollection.getSubjectiveQuestionsId().get(j) + ",";// ,号分隔
				}
				pStatement.setString(6, subjectiveStr);
			} else {
				pStatement.setString(6, null);
			}
			if (exerciseCollection.getProgrammingQuestionsId().size() != 0) {
				String programmingStr = null;
				for (int j = 0; j < exerciseCollection.getProgrammingQuestionsId().size(); j++) {
					programmingStr = exerciseCollection.getProgrammingQuestionsId().get(j) + ",";// ,号分隔
				}
				pStatement.setString(7, programmingStr);
			} else {
				pStatement.setString(7, null);
			}
			// 设置SQL语句参数
			pStatement.setString(1, exerciseCollection.getName());
			pStatement.setInt(2, exerciseCollection.getScoreChoice());
			pStatement.setInt(3, exerciseCollection.getScoreJudgment());

			int rn = pStatement.executeUpdate();// 将当前参数加入对象列表
			if (rn == 0) {// 更新记录无，插入失败
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 插入成功
		return true;
	}

	/**
	 * 基于 MySql 数据库实现的deleteData（） 方法， 该方法用于向数据库中删除题集信息。 可以根据题集的ID|题集名称进行删除
	 * 
	 * @param
	 * @return 删除是否都成功 boolean
	 */
	@Override
	public boolean deleteData(ExerciseCollections exerciseCollection) {
		int rn = 0;
		Connection con = null; // 创建 Connection 数据库连接对象
		PreparedStatement pStatement = null;// 预编译的SQL语句对象
		String strSql1 = "delete from Collections where Id = ? ;";
		String strSql2 = "delete from Collections where name = ? ;";

		try {
			con = MysqlConnect.connectDatabase();
			if (exerciseCollection.getId() != null) {// 根据Id查
				pStatement = con.prepareStatement(strSql1);
				pStatement.setInt(1, exerciseCollection.getId());
			} else {// 根据题集名称查
				pStatement = con.prepareStatement(strSql2);
				pStatement.setString(1, exerciseCollection.getName());
			}
			rn = pStatement.executeUpdate();
			if (rn == 0) {// 删除失败
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		} // 删除成功
		return true;
	}
}
