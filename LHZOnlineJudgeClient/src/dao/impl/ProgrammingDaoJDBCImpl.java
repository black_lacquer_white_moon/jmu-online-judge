package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.ProgrammingDAO;
import model.ProgrammingQuestion;
import util.MysqlConnect;

/**
 * 基于 MySQL 实现的ProgrammingDAO接口
 * 
 * @author ZML
 * @version 1.0
 */

public class ProgrammingDaoJDBCImpl implements ProgrammingDAO {
	/**
	 * 基于 MySql 数据库实现的getData（） 方法， 该方法用于获取数据库中的编程类型题集,返回List的该对象集。
	 * 
	 * @param programmingQuestionsId 编程题号序列，List<Integer>
	 * @return 编程题对象集 ：List<ProgrammingQuestion>
	 */
	@Override
	public List<ProgrammingQuestion> getData(List<Integer> programmingQuestionsId) { // 获取选择题对象
		List<ProgrammingQuestion> programmingQuestions = new ArrayList<>();// 要返回的对象集
		Connection con = null; // 创建 Connection 数据库连接对象
		ResultSet rs = null; // 创建 ResultSet 结果集对象
		PreparedStatement pStatement = null;// 预编译的SQL语句对象
		String strSql = "select*from program where Id=?;";

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			// 在表中根据编程题号依次查找
			for (Integer i : programmingQuestionsId) {
				pStatement.setInt(1, i);// 设置要查找的题号
				rs = pStatement.executeQuery();

				while (rs.next()) {
					// 获取该题目对象
					ProgrammingQuestion p = new ProgrammingQuestion();
					p.setId(rs.getInt("Id"));
					p.setText(rs.getString("text"));
					p.setTitle(rs.getString("title"));
					p.setInput(rs.getString("Input"));
					p.setOutput(rs.getString("output"));
					p.setTestpoint(rs.getInt("testpoint"));
					p.setScore(rs.getInt("score"));
					// 存储进编程题对象列表
					programmingQuestions.add(p);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(rs);
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 返回获取的编程题对象列表
		return programmingQuestions;
	}

	/**
	 * 基于 MySql 数据库实现的storeData（） 方法， 该方法用于向数据库中插入设置的编程题信息，存储数据
	 * 
	 * @param List<ProgrammingQuestion> 判断题对象集，List<ProgrammingQuestion>
	 * @return 插入是否都成功 boolean
	 */
	@Override
	public boolean storeData(List<ProgrammingQuestion> programmingQuestions) {
		Connection con = null; // 创建 Connection 数据库连接对象
		PreparedStatement pStatement = null;
		String strSql = "insert into program(title,text,Input,output,testpoint,score) values(?,?,?,?,?,?);";

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			// 遍历所有主观题对象
			for (ProgrammingQuestion p : programmingQuestions) {
				// 设置SQL语句参数
				pStatement.setString(1, p.getTitle());
				pStatement.setString(2, p.getText());
				pStatement.setString(3, p.getInput());
				pStatement.setString(4, p.getOutput());
				pStatement.setInt(5, p.getTestpoint());
				pStatement.setInt(6, p.getScore());

				pStatement.addBatch();// 将当前参数加入对象列表
			}
			int[] num = pStatement.executeBatch();// 将一批命令提交给数据库执行
			for (int i : num) {
				if (i == 0) {// 若有出现插入失败的题目则返回false
					return false;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		// 全部插入成功返回true
		return true;
	}

	/**
	 * 基于 MySql 数据库实现的deleteData（） 方法， 该方法用于向数据库中删除编程题信息。 当错误信息，重新设置时可能用到
	 * 
	 * @param programmingQuestionsId 要删除的选择题题号序列，List<Integer>
	 * @return 删除是否都成功 boolean
	 */
	@Override
	public boolean deleteData(List<Integer> programmingQuestionsId) {
		int[] num;
		Connection con = null; // 创建 Connection 数据库连接对象
		PreparedStatement pStatement = null;// 预编译的SQL语句对象
		String strSql = "delete from  program where Id=?;";

		try {
			con = MysqlConnect.connectDatabase();
			pStatement = con.prepareStatement(strSql);
			// 在表中根据选择题题号依次查找
			for (Integer i : programmingQuestionsId) {
				pStatement.setInt(1, i);// 设置要查找的题号
				pStatement.addBatch();
			}
			num = pStatement.executeBatch();// 批量处理命令
			for (int i : num) {
				if (i == 0) {
					return false;

				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(pStatement);
			MysqlConnect.close(con);
		}
		return true;
	}
}
