package dao;

import java.util.List;

import model.SubjectiveQuestion;
/**
* ChoiceDAO 接口指定主观题的相关方法
* @author ZML
* @version 1.0
*/

public interface SubjectiveDAO {
	/**
	   * 这个方法用于从数据库中获取主观题信息，得到主观题对象
	   * @param subjectiveQuestionsId主观题题号集，List
	   * @return 主观题对象集 List<SubjectiveQuestion> 
	   */
	public List<SubjectiveQuestion> getData(List<Integer> subjectiveQuestionsId);
	/**
	   * 这个方法用于向数据库中存储 主观题该类的对象内容
	   * @param subjectiveQuestions 主观题对象集，List<SubjectiveQuestion>
	   * @return 是否全部插入成功，boolean
	   */
	public  boolean storeData(List<SubjectiveQuestion> subjectiveQuestions) ;
	
	/**
	   * 这个方法用于向数据库中 批量删除 主观题题内容，
	   * @param subjectiveQuestionsId 要删除的题目ID序列，List 
	   * @return 是否全部删除成功，boolean
	   */
	public  boolean deleteData(List<Integer> subjectiveQuestionsId);
}
