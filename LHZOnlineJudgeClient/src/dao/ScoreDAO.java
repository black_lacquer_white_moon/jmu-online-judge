package dao;

import java.util.List;

import model.Score;
/**
*ScoreDAO 接口指定成绩的相关方法
* @author ZML
* @version 1.0
*/

public interface ScoreDAO {
	/**
	 * 基于 MySql 数据库实现的getClassAllScore（） 方法， 该方法用于老师获取某班级某题集的所有同学的超级
	 * 
	 * @param classId      班级id名称,String
	 * @param collectionId 题集id,Integer
	 * @return 题集id序列，List<Integer>
	 */

	public List<Score> getClassAllScore(String classId, Integer collectionId) ;
	/**
	 * 基于 MySql 数据库实现的getClassAllScore（） 方法， 该方法用于学生获取自己已做题集的所有成绩
	 * 
	 * @param username      学生名字，String
	 * @param collectionId 题集id名称,Integer。主要应传入的是所有
	 * @return 题集id序列，List<Integer>
	 */
	public List<Score> getDoneCollectionScore(String username, List<Integer> collectionId) ;
	
	/**
	 * 基于 MySql 数据库实现的storeData（） 方法。可批量添加成绩记录
	 * 
	 * @param List<Score> scoreList
	 * @return 插入是否都成功 boolean
	 */

	public boolean storeData(List<Score> scoreList);
}
