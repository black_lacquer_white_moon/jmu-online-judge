package dao;

import java.util.List;

import model.JudgmentQuestion;

/**
* ChoiceDAO 接口指定判断题的相关方法
* @author ZML
* @version 1.0
*/

public interface JudgmentDAO {
	/**
	   * 这个方法用于从数据库中获取判断题信息，得到判断题对象
	   * @param judgementQuestionsId 判断题题号集，List
	   * @return 判断题对象集 ，List<JudgementQuestion>
	   */
	public List<JudgmentQuestion> getData(List<Integer> judgementQuestionsId) ;
	/**
	   * 这个方法用于向数据库中存储 判断题该类的对象内容
	   * @param judgementQuestions 判断题对象集，List
	   * @return 是否全部插入成功，boolean
	   */
	public boolean storeData(List<JudgmentQuestion> judgementQuestions) ;
	/**
	   * 这个方法用于向数据库中 批量删除 判断题题内容，
	   * @param judgementQuestionsId 要删除的题目ID序列，List 
	   * @return 是否全部删除成功，boolean
	   */
	public  boolean deleteData(List<Integer> judgementQuestionsId);
}
