package dao;

import java.util.List;
import model.ChoiceQuestion;

/**
* ChoiceDAO 接口指定 选择题的相关方法
* @author ZML
* @version 1.0
*/


public interface ChoiceDAO {
	/**
	   * 这个方法用于从数据库中 获取所有需要的选择题信息，得到选择题对象
	   * @param choiceQuestionsId 选择题题号集，List<Integer>
	   * @return 选择题对象集 ， List<ChoiceQuestion>
	   */
	public List<ChoiceQuestion> getData(List<Integer> choiceQuestionsId);
	/**
	   * 这个方法用于向数据库中 批量存储 选择题该类的对象内容
	   * @param choiceQuestions 选择题对象集，List<Integer>
	   * @return 是否全部插入成功，boolean
	   */
	public boolean storeData(List<ChoiceQuestion> choiceQuestions) ;
	/**
	   * 这个方法用于向数据库中 批量删除 选择题内容，
	   * @param choiceQuestionsId 要删除的题目ID序列，List<Integer>
	   * @return 是否全部删除成功，boolean
	   */
	public boolean deleteData(List<Integer> choiceQuestionsId) ;
}
