package dao;

import model.ExerciseCollections;
/**
 * ExerciseCollectionsDAO接口 指定题目集与数据库相交互的方法
 * @author ZML
 * @version 1.1
 */

public interface ExerciseCollectionsDAO {
	/**
	 *  用来获取习题集内所有题目Id的方法。可以通过题集的Id|题集的名称来进行搜索获取
	 * @param exerciseCollection 题目集对象，ExerciseCollections
	 * @return boolean 获取题集Id数据情况是否成功
	 */
	public boolean getData(ExerciseCollections exerciseCollection) ;
	/**
	 *  根据题目Id,调用相关类型题目方法，获得题目对象集
	 * @param exerciseCollection 题目集对象，ExerciseCollections
	 * @return boolean 获取题集数据情况是否成功
	 */
	public void getExercise(ExerciseCollections exerciseCollection);
	/**
	 * 存储题集的Id集，即数据库中存储题集有什么题目
	 * @param exerciseCollection 题目集对象，ExerciseCollections
	 * @return boolean 插入记录是否成功
	 */
	public boolean storeData(ExerciseCollections exerciseCollection) ;
	
	/**
	 *  该方法用于向数据库中删除题集信息。 可以根据题集的ID或是题集名称进行删除
	 * @param subjectiveQuestionsId 要删除的选择题题号序列，List<Integer>
	 * @return 删除是否都成功 boolean
	 */
	public boolean deleteData(ExerciseCollections exerciseCollection) ;

}
