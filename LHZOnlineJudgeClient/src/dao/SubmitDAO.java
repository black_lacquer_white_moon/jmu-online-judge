package dao;

import java.sql.SQLException;

/**
* SubmitDAO 接口包含同学提交答案的方法
* @author 林智凯
* @version 1.0
*/
public interface SubmitDAO {

	/**
	   * 该方法用于提交一道主观题
	   * @param username 用户名(非加密)，String
	   * @param questiondId 主观题题号，Integer
	   * @param text 主观题作答，string
	   * @return boolean：提交成功true，反之为false。
	   * @throws SQLException 数据库异常
	   */
	public boolean submitSubjective(String username, Integer questiondId, String text) throws SQLException;
	
	/**
	   * 该方法用于提交一个题集的选择题
	   * @param username 用户名(非加密)，String
	   * @param classId 班级id，Integer
	   * @param collectionId 题目集id，Integer
	   * @param text 提交内容，String，选择题为逗号分隔的答案字符串
	   * @return boolean：提交成功true，反之为false。
	   */
	public String submitChoice(String username, String classId, Integer collectionId, String text);
	
	/**
	   * 该方法用于提交一个题集的判断题
	   * @param username 用户名(非加密)，String
	   * @param classId 班级id，Integer
	   * @param collectionId 题目集id，Integer
	   * @param text 提交内容，String，选择题为逗号分隔的答案字符串
	   * @return boolean：提交成功true，反之为false。
	   */
	public String submitJudgment(String username, String classId, Integer collectionId, String text);
	
	/**
	   * 该方法用于提交一道编程题
	   * @param username 用户名(非加密)，String
	   * @param classId 班级id，Integer
	   * @param collectionId 题目集id，Integer
	   * @param questionId 编程题id，Integer
	   * @param code 提交的代码，String
	   * @return boolean：提交成功true，反之为false。
	   */
	public String submitProgram(String username, String classId, Integer collectionId, Integer questionId, String code);
}
