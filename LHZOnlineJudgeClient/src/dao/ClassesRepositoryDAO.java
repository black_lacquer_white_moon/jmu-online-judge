package dao;

import java.util.List;

/**
 * ClassesRepositoryDAO接口指定班级管理记录的相关方法
 * 
 * @author ZML
 * @version 1.0
 */

public interface ClassesRepositoryDAO {

	/**
	 * 这个方法用于 存储班级管理记录
	 * 
	 * @param username  用户名称，String
	 * @param type      用户类型，Integer，0为学生|1为老师
	 * @param classname 班级名称，String
	 * @return boolean 插入情况是否成功
	 */
	public  boolean storeData(String username, Integer type, String classname);

	/**
	 * 这个方法用于查询学生可选择加入的班级。 用于查询学生可加入的班级。
	 * 
	 * @param username 班级名称 ，String
	 * @return 返回可加入的班级名称集 List
	 */
	public  List<String> searchUnjoinedClass(String username);

	/**
	 * 通常用于查询 老师创建过哪些班级？ 同学加入过哪些班级？
	 * 
	 * @param username 用户名称，String
	 * @return 班级名称集 List
	 */

	public List<String> searchByUsername(String username) ;

	/**
	 * 这个方法 根据班级名称进行查询 查询 班级是否存在？老师不能创建已存在的班级| 学生不能加入不存在的班级 时调用
	 * 
	 * @param classname 班级名称，String
	 * @return boolean 返回是否存在
	 */
	public  boolean searchByClassname(String classname) ;

	/**
	 * 这个方法用于 删除某班级中某个学生 删除某班某同学，删除的是单条记录
	 * 
	 * @param classname 班级名称，String
	 * @param username  班级名称，String
	 * @return boolean 返回删除情况是否完全成功
	 */
	public  boolean deleteStudent(String username, String classname) ;

	/**
	 * 用于删除整个班级 ，删除关于该班级的相关记录，是批量删除
	 * 
	 * @param classname 班级名称，String
	 * @return boolean 返回删除情况是否完全成功
	 */
	public  boolean deleteClass(String classname) ;
}
