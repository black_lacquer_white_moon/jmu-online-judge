package model;

/**
 * JudgementQuestion类为判断题对象,继承自题目类Exercise。
 * 特有属性有正确答案answer
 * @author ZML
 * @version 1.0
 */
public class JudgmentQuestion extends Exercise {
	private String answer;

	public JudgmentQuestion() {
		super();
	}

	/**
	 * 这个方法是JudgementQuestion对象的有参构造器
	 * @param text   题干，String
     * @param answer 答案,String
	 */
	public JudgmentQuestion( String text, String answer) {
		super(text);
		//id数据库自动生成
		this.answer=answer;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

}
