package model;

/**
* Testpoint 测试点类负责存储编程题的测试点
* @author 林智凯
* @version 1.0
*/
public class Testpoint {
	/**输入样例*/
	private String input;    
	/**输出样例*/
	private String output;    
	
	 /**
	   * Testpoint类的构造方法
	   * @param String input:输入样例
	   * @param String output:输出样例
	   */
	public Testpoint(String input, String output) {
		this.input = input;
		this.output = output;
	}

	/**
	   * input 字段访问器
	   * @return String input:输入样例
	   */
	public String getInput() {
		return input;
	}

	/**
	   * output 字段访问器
	   * @return String output:输出样例
	   */
	public String getOutput() {
		return output;
	}

	/**
	   * input 字段修改器
	   * @param String input:输入样例
	   */
	public void setInput(String input) {
		this.input = input;
	}

	/**
	   * output 字段修改器
	   * @param String output:输出样例
	   */
	public void setOutput(String output) {
		this.output = output;
	}
	
}
