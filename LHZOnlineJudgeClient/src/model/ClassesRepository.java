package model;
/**
 * ClassesRepository类用于存储班级管理
 * 
 * @author ZML
 * @version 1.0
 */
public class ClassesRepository {
	private Integer id;
	private String username;
	private Integer type;
	private String classname;

	public ClassesRepository() {
		super();
	}

	/**
	   * 这个方法是 ClassesRepository对象的有参构造器
	   * @param id 题目编号，Integer
	   * @param username 用户名称，String                        
	   * @param type 用户类型,Integer
	   * @param classname 班级名称,String
	   */
	public ClassesRepository(Integer id, String username, Integer type, String classname) {
		super();
		this.id = id;
		this.username = username;
		this.type = type;
		this.classname = classname;
	}

	public int getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getClassname() {
		return classname;
	}

	public void setClassname(String classname) {
		this.classname = classname;
	}

}
