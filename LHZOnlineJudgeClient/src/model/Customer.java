package model;

import util.Md5Util;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import dao.impl.UserDaoImpl;

/**
* 这个类存储了用户名及其MD5加密，用户类型和其所属班级列表
* @author 林智凯
* @version 1.2
*/
public class Customer {
	/**用户名*/
	private final String username;    
	/**用户名MD5加密*/
	private final String usernameMd5;   
	/**用户类型，0为学生，1为教师*/
	private final Integer type;    
	/**班级类列表*/
	private List<Classes> classslist;    
	
	/**
	   * 这个方法是customer对象的构造器
	   * @param username 用户名，String
	   * @return customer对象
	 * @throws SQLException 
	   */
	public Customer(String username) throws SQLException {
		this.username = username;
		this.usernameMd5 = Md5Util.getMd5Str(username);
		if(UserDaoImpl.checkType(this.getUsernameMd5())) {
			//教师
			this.type = 1;    
		}
		else {
			//学生
			this.type = 0;    
		}
	}
	
	/**
	   * 访问字段username，用户名
	   * @return username 用户名
	   */
	public String getUsername() {
		return username;
	}

	/**
	   * 访问字段username_md，加密后的用户名
	   * @return username_md 加密后的用户名
	   */
	public String getUsernameMd5() {
		return usernameMd5;
	}

	/**
	   * 访问字段type，用户类型
	   * @return 用户类型，0为学生，1为教师
	   */
	public Integer getType() {
		return type;
	}

	/**
	   * 访问字段classslist，班级列表
	   * @return List<Classes>
	   */
	public List<Classes> getClassslist() {
		return classslist;
	}

	/**
	   * 修改字段classslist，班级列表
	   */
	public void setClassslist(LinkedList<Classes> classslist) {
		this.classslist = classslist;
	}
}
