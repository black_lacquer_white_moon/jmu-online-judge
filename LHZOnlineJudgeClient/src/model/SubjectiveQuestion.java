package model;

/**
 * SubjectiveQuestion 类为选择题对象,继承自题目类Exercise,。
 * 特有属性:题目分值score
 * @author ZML
 * @version 1.0
 */
public class SubjectiveQuestion extends Exercise {
	private Integer score;

	public SubjectiveQuestion() {
		super();
	}

	/**
	 * 这个方法是SubjectiveQuestion对象的有参构造器
	 * @param text   题干，String
	 * @param score  选项,Integer
	 */
	public SubjectiveQuestion( String text, Integer score) {
		super( text);
		//id数据库自动生成
		this.score=score;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}


}
