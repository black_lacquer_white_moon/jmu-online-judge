package model;

/**
 * Exercise类为习题对象。
 * 基本属性包含题目编号，题干信息
 * @author ZML
 * @version 1.0
 */

public class Exercise {
	private Integer id;// 题目编号
	private String text;// 题干信息

	public Exercise() {
		super();
	}
	/**
	 * 这个方法是Exercise对象的有参构造器
	 * @param text   题干，String
	 */

	public Exercise(String text) {
		super();
		//题目编号自动生成
		this.text = text;
	}

	public int getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}