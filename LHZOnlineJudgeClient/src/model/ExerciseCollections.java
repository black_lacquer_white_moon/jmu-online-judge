package model;

import java.util.List;

import dao.impl.ExerciseCollectionsDaoJDBCImpl;

/**
 * ExerciseCollections 类习题集对象 。
 * 属性包括题集编号，题集名称，选择题统一分值，判断题统一分值，不同类型题目的编号序列，以及不同题目对象序列。
 * 
 * @author ZML
 * @version 1.1
 */
public class ExerciseCollections {

	private Integer id;// 题集编号
	private String name;// 题集名称

	private Integer scoreChoice;// 选择题分值
	private Integer scoreJudgment;// 判断题分值

	// 不同类型题目编号序列
	private List<Integer> choiceQuestionsId;
	private List<Integer> judgmentQuestionsId;
	private List<Integer> programmingQuestionsId;
	private List<Integer> subjectiveQuestionsId;

	// 不同类型题目对象序列
	private List<ChoiceQuestion> choiceQuestions;
	private List<JudgmentQuestion> judgmentQuestions;
	private List<ProgrammingQuestion> programmingQuestions;
	private List<SubjectiveQuestion> subjectiveQuestions;

	public ExerciseCollections() {
		super();
	}

	/**
	 * 这个方法是JudgementQuestion对象的有参构造器。 创建题集的常用方法
	 * 
	 * @param name     题集名称，String
	 * @param 各类型题目编号集 List<Integer>
	 */
	public ExerciseCollections(String name, Integer scoreChoice, Integer scoreJudgment, List<Integer> choiceQuestionsId,
			List<Integer> judgmentQuestionsId, List<Integer> programmingQuestionsId,
			List<Integer> subjectiveQuestionsId) {
		super();
		// 题目id数据库自动生成
		this.name = name;
		this.scoreChoice = scoreChoice;
		this.scoreJudgment = scoreJudgment;
		this.choiceQuestionsId = choiceQuestionsId;
		this.judgmentQuestionsId = judgmentQuestionsId;
		this.programmingQuestionsId = programmingQuestionsId;
		this.subjectiveQuestionsId = subjectiveQuestionsId;
		// 创建一个题集对象时即记录进数据库
		new ExerciseCollectionsDaoJDBCImpl().storeData(this);
	}

	public ExerciseCollections(Integer id, String name, Integer scoreChoice, Integer scoreJudgment,
			List<Integer> choiceQuestionsId, List<Integer> judgmentQuestionsId, List<Integer> programmingQuestionsId,
			List<Integer> subjectiveQuestionsId, List<ChoiceQuestion> choiceQuestions,
			List<JudgmentQuestion> judgmentQuestions, List<ProgrammingQuestion> programmingQuestions,
			List<SubjectiveQuestion> subjectiveQuestions) {
		super();
		this.id = id;
		this.name = name;
		this.scoreChoice = scoreChoice;
		this.scoreJudgment = scoreJudgment;
		this.choiceQuestionsId = choiceQuestionsId;
		this.judgmentQuestionsId = judgmentQuestionsId;
		this.programmingQuestionsId = programmingQuestionsId;
		this.subjectiveQuestionsId = subjectiveQuestionsId;
		this.choiceQuestions = choiceQuestions;
		this.judgmentQuestions = judgmentQuestions;
		this.programmingQuestions = programmingQuestions;
		this.subjectiveQuestions = subjectiveQuestions;
	}

	public Integer getScoreChoice() {
		return scoreChoice;
	}

	public void setScoreChoice(Integer scoreChoice) {
		this.scoreChoice = scoreChoice;
	}

	public Integer getScoreJudgment() {
		return scoreJudgment;
	}

	public void setScoreJudgment(Integer scoreJudgment) {
		this.scoreJudgment = scoreJudgment;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Integer> getChoiceQuestionsId() {
		return choiceQuestionsId;
	}

	public void setChoiceQuestionsId(List<Integer> choiceQuestionsId) {
		this.choiceQuestionsId = choiceQuestionsId;
	}

	public List<Integer> getJudgmentQuestionsId() {
		return judgmentQuestionsId;
	}

	public void setJudgmentQuestionsId(List<Integer> judgmentQuestionsId) {
		this.judgmentQuestionsId = judgmentQuestionsId;
	}

	public List<Integer> getProgrammingQuestionsId() {
		return programmingQuestionsId;
	}

	public void setProgrammingQuestionsId(List<Integer> programmingQuestionsId) {
		this.programmingQuestionsId = programmingQuestionsId;
	}

	public List<Integer> getSubjectiveQuestionsId() {
		return subjectiveQuestionsId;
	}

	public void setSubjectiveQuestionsId(List<Integer> subjectiveQuestionsId) {
		this.subjectiveQuestionsId = subjectiveQuestionsId;
	}

	public List<ChoiceQuestion> getChoiceQuestions() {
		return choiceQuestions;
	}

	public void setChoiceQuestions(List<ChoiceQuestion> choiceQuestions) {
		this.choiceQuestions = choiceQuestions;
	}

	public List<JudgmentQuestion> getJudgmentQuestions() {
		return judgmentQuestions;
	}

	public void setJudgmentQuestions(List<JudgmentQuestion> judgmentQuestions) {
		this.judgmentQuestions = judgmentQuestions;
	}

	public List<ProgrammingQuestion> getProgrammingQuestions() {
		return programmingQuestions;
	}

	public void setProgrammingQuestions(List<ProgrammingQuestion> programmingQuestions) {
		this.programmingQuestions = programmingQuestions;
	}

	public List<SubjectiveQuestion> getSubjectiveQuestions() {
		return subjectiveQuestions;
	}

	public void setSubjectiveQuestions(List<SubjectiveQuestion> subjectiveQuestions) {
		this.subjectiveQuestions = subjectiveQuestions;
	}

}
