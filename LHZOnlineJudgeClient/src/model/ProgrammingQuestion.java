package model;
/**
*ProgrammingQuestion 类为编程题对象,继承自题目类Exercise
* @author 林智凯
* @version 1.0
*/
public class ProgrammingQuestion extends Exercise {
	private Integer score;
	/**题目标题*/
	private String title;    
	/**输入描述*/
	private String input;   
	/**输出描述*/
	private String output;   
	/**是否有测试点，1有0没有*/
	private Integer testpoint;    
	
	public ProgrammingQuestion() {
		super();
	}
	/**
	   * ProgrammingQuestion对象的有参构造器
	   * @param text 题干，String                        
	   * @param score 题目分值, Integer
	   * @param title 题目标题,String
	   * @param input 输入描述，String
	   * @param output 输出描述,String
	   * @param testpoint 是否有测试点, Integer
	   */
	public ProgrammingQuestion(String text, Integer score, String title , String input, String output, Integer testpoint) {
		super( text);
		//id数据库自动生成
		this.score=score;
		this.title = title;
		this.input = input;
		this.output = output;
		this.testpoint = testpoint;
	}
	
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public String getTitle() {
		return title;
	}
	
	public String getInput() {
		return input;
	}
	
	public String getOutput() {
		return output;
	}
	
	public Integer getTestpoint() {
		return testpoint;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setInput(String input) {
		this.input = input;
	}
	
	public void setOutput(String output) {
		this.output = output;
	}
	
	public void setTestpoint(Integer testpoint) {
		this.testpoint = testpoint;
	}
}
