package model;

import java.util.ArrayList;
import java.util.List;
/**
* ChoiceQuestion 类为选择题对象,继承自题目类Exercise
* 特有属性选项options,答案answer
* @author ZML
* @version 1.1
*/

public class ChoiceQuestion extends Exercise {
	private String answer;
	private List<String> options;
	
	public ChoiceQuestion( ) {
		super();
		this.options=new ArrayList<>();
	}

	/**
	   * 这个方法是ChoiceQustion对象的有参构造器
	   * @param text 题干，String                        
	   * @param answer 答案,String
	   * @param options 选项,List<String>
	   */
	public ChoiceQuestion(String text,String answer, List<String> options) {
		super(text);
		this.options = options;
		this.answer=answer;
	}
	
	public List<String> getOptions() {
		return options;
	}

	public void setOptions(List<String> options) {
		this.options = options;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	
}
