package model;

import java.util.List;

/**
* Classes 存储班级id，班级名和班级拥有的题目集
* @author 林智凯
* @version 1.0
*/
public class Classes {
	/**班级id*/
	private Integer id;
	/**班级名称*/
	private String className;    
	/**班级拥有的题目集id*/
	private List<Integer> collentionsList;    
	
	/**
	 * Classes对象的有参构造器
	 * @param id Integer 班级id
	 * @param className String 班级名称
	 */
	public Classes(Integer id, String className) {
		this.id = id;
		this.className = className;
	}

	public Integer getId() {
		return id;
	}

	public String getClassName() {
		return className;
	}

	public List<Integer> getCollentionsList() {
		return collentionsList;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public void setCollentionsList(List<Integer> collentionsList) {
		this.collentionsList = collentionsList;
	}
	
}
