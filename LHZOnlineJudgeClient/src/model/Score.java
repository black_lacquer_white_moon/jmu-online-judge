package model;

/**
*Score 类为成绩单对象，存储用户
* @author 林智凯
* @version 1.0
*/
public class Score {
	/**作答学生*/
	private String username;    
	/**题目集id*/
	private Integer collectionId;  
	/**选择题分数*/
	private Integer choice;    
	/**判断题分数*/
	private Integer judgment;    
	/**编程题分数*/
	private Integer programming;    
	/**主观题分数*/
	private Integer subjective;    
	/**班级名*/
	private String classId;    
	/**总分*/
	private Integer total;    
	
	/**
	 * Score对象的有参构造器
	 * @param username String 作答学生
	 * @param collectionId Integer 题目集id
	 * @param choice Integer 选择题分数
	 * @param judgment Integer 判断题分数
	 * @param programming Integer 编程题分数
	 * @param subjective Integer 主观题分数
	 * @param classId String 班级名
	 */
	public Score(String username, Integer collectionId, Integer choice, Integer judgment, Integer programming, Integer subjective, String classId) {
		this.username = username;
		this.collectionId = collectionId;
		this.choice = choice;
		this.judgment = judgment;
		this.programming = programming;
		this.subjective = subjective;
		this.classId = classId;
		this.total = choice + judgment + programming + subjective;
	}
	
	public String getUsername() {
		return username;
	}
	
	public Integer getCollectionId() {
		return collectionId;
	}
	
	public Integer getChoice() {
		return choice;
	}
	
	public Integer getJudgment() {
		return judgment;
	}
	
	public Integer getProgramming() {
		return programming;
	}
	
	public Integer getSubjective() {
		return subjective;
	}
	
	public String getClassId() {
		return classId;
	}
	
	public Integer getTotal() {
		return total;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setCollectionId(Integer collectionId) {
		this.collectionId = collectionId;
	}
	
	public void setChoice(Integer choice) {
		this.choice = choice;
	}
	
	public void setJudgment(Integer judgment) {
		this.judgment = judgment;
	}
	
	public void setProgramming(Integer programming) {
		this.programming = programming;
	}
	
	public void setSubjective(Integer subjective) {
		this.subjective = subjective;
	}
	
	public void setClassId(String classId) {
		this.classId = classId;
	}
	
	public void setTotal(Integer total) {
		this.total = this.choice + this.judgment + this.programming + this.subjective;
	}
}
