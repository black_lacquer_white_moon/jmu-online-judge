package ui.controller;

import dao.impl.UserDaoImpl;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


import java.io.IOException;
import java.util.Optional;

public class ChangePasswordController {
    @FXML
    private AnchorPane root;
    @FXML
    private PasswordField oldPassword;
    @FXML
    private PasswordField newPassword;
    @FXML
    private PasswordField newPassword2;

    public void clickCancel(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) root.getScene().getWindow();
        //stage.close();
        Parent root = FXMLLoader.load(getClass().getResource("../recourse/fxml/stuSetting.fxml"));
        stage.setScene(new Scene(root, 202, 240));
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }

    public void clickConfirm(MouseEvent mouseEvent) throws IOException{
        if (newPassword.getText().isEmpty() | newPassword2.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "密码不能为空");
        } else if (!newPassword.getText().equals(newPassword2.getText())) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "两次输入的密码不同");
        } else if (UserDaoImpl.changePassword(LoginController.customer.getUsernameMd5(), oldPassword.getText(), newPassword.getText())){
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "改密成功,请重新登录",
                    new ButtonType("确认", ButtonBar.ButtonData.YES));
            Optional<ButtonType> buttonType = alert.showAndWait();
            if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
                Stage stage = (Stage) root.getScene().getWindow();
                //stage.close();
                stage.centerOnScreen();
                Parent root = FXMLLoader.load(getClass().getResource("../recourse/fxml/login.fxml"));
                stage.setScene(new Scene(root, 328, 238));
                stage.setResizable(false);
                stage.show();
            }
        } else {
            System.out.println("wrong");
        }
    }
}
