package ui.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class StuMenuController {
    public Button classButton;
    @FXML
    private AnchorPane menu;

    public void clickSetting(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) menu.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("../recourse/fxml/stuSetting.fxml"));
        stage.setScene(new Scene(root, 202, 240));
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }

    public void clickLogout(MouseEvent mouseEvent) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "是否退出登录？",
                new ButtonType("确认", ButtonBar.ButtonData.YES),
                new ButtonType("取消",ButtonBar.ButtonData.NO));
        Optional<ButtonType> buttonType = alert.showAndWait();
        if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
            Stage stage = (Stage) menu.getScene().getWindow();
            Parent root = FXMLLoader.load(getClass().getResource("../recourse/fxml/login.fxml"));
            stage.setScene(new Scene(root, 328, 238));
            stage.centerOnScreen();
            stage.setResizable(false);
            stage.show();
        }

    }

    public void clickEmail(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) menu.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("../recourse/fxml/email.fxml"));
        stage.setScene(new Scene(root, 600, 400));
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }

    public void clickJoin(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) menu.getScene().getWindow();
        stage.centerOnScreen();
        Parent root = FXMLLoader.load(getClass().getResource("../recourse/fxml/joinClass.fxml"));
        stage.setScene(new Scene(root, 600, 400));
        stage.setResizable(false);
        stage.show();
    }

    public void clickClass(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) menu.getScene().getWindow();
        stage.centerOnScreen();
        Parent root = FXMLLoader.load(getClass().getResource("../recourse/fxml/class.fxml"));
        stage.setScene(new Scene(root, 600, 400));
        stage.setResizable(false);
        stage.show();
    }
}
