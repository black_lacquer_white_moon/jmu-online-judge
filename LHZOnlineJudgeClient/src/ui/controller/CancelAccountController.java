package ui.controller;

import dao.impl.UserDaoImpl;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class CancelAccountController {
    public AnchorPane root;
    public Button confirm;
    public Button cancel;
    public PasswordField password;

    public void clickConfirm(MouseEvent mouseEvent) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "是否注销账户？",
                new ButtonType("确认", ButtonBar.ButtonData.YES),
                new ButtonType("取消",ButtonBar.ButtonData.NO));
        Optional<ButtonType> buttonType = alert.showAndWait();
        if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
            if (UserDaoImpl.cancelUser(LoginController.customer.getUsernameMd5(), password.getText())) {
                Alert alert1 = new Alert(Alert.AlertType.INFORMATION, "该账户已注销！",
                        new ButtonType("确认", ButtonBar.ButtonData.YES));
                Optional<ButtonType> buttonType1 = alert1.showAndWait();
                if (buttonType1.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
                    Stage stage = (Stage) root.getScene().getWindow();
                    stage.close();
                    stage.centerOnScreen();
                    Parent root = FXMLLoader.load(getClass().getResource("../recourse/fxml/login.fxml"));
                    stage.setScene(new Scene(root, 328, 238));
                    stage.setResizable(false);
                    stage.show();
                }
            } else {
                Alert alert1 = new Alert(Alert.AlertType.INFORMATION, "密码错误，请重新输入！");
                alert1.show();
            }
        }

    }

    public void clickCancel(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) root.getScene().getWindow();
        //stage.close();
        Parent root = FXMLLoader.load(getClass().getResource("../recourse/fxml/stuSetting.fxml"));
        stage.setScene(new Scene(root, 202, 240));
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }
}
