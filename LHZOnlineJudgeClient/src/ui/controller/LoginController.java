package ui.controller;

import dao.impl.UserDaoImpl;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Customer;

import java.io.IOException;
import java.sql.SQLException;

public class LoginController{
    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    @FXML
    private AnchorPane root;

    public static Customer customer;

    public void loginOnAction(MouseEvent mouseEvent) throws IOException, SQLException {
        if (username.getText().isEmpty() | password.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "用户名和/或密码不能为空");
            alert.setTitle("登录失败");
            alert.show();
        }  else if (UserDaoImpl.signIn(username.getText(), password.getText())) {
            customer = new Customer(username.getText());
            Stage stage = (Stage) root.getScene().getWindow();
            stage.close();
            Parent root = FXMLLoader.load(getClass().getResource("../recourse/fxml/stuMenu.fxml"));
            stage.setScene(new Scene(root, 447, 466));
            stage.setResizable(false);
            stage.centerOnScreen();
            stage.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING, "用户名和/或密码错误");
            alert.setTitle("登录失败");
            alert.show();
        }
    }

    public void logonOnAction(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) root.getScene().getWindow();
        stage.centerOnScreen();
        Parent root = FXMLLoader.load(getClass().getResource("../recourse/fxml/logon.fxml"));
        stage.setScene(new Scene(root, 343, 249));
        stage.setResizable(false);
        stage.show();
    }
}
