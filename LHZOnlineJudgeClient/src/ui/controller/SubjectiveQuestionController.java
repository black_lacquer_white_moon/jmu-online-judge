package ui.controller;

import dao.impl.ExerciseCollectionsDaoJDBCImpl;
import dao.impl.SubjectiveDaoJDBCImpl;
import dao.impl.SubmitDaoSocketImpl;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.ExerciseCollections;
import model.SubjectiveQuestion;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class SubjectiveQuestionController implements Initializable {
    public Button last;
    public Button next;
    public Label stem;
    public Label tittle;
    public TextArea content;
    @FXML
    private AnchorPane root;
    int idx = 0;
    URL url;
    ResourceBundle resourceBundle;
    List<SubjectiveQuestion> subQuestionList;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        url = location;
        resourceBundle =resources;
        content.setWrapText(true);
        content.clear();
        ExerciseCollections ecByName = new ExerciseCollections();
        ecByName.setName(ClassProblemSetController.questionSetName);
        (new ExerciseCollectionsDaoJDBCImpl()).getData(ecByName);
        subQuestionList =
                (new SubjectiveDaoJDBCImpl()).getData(ecByName.getSubjectiveQuestionsId());
        tittle.setText("主观题");
        stem.setText(subQuestionList.get(idx).getText());
    }

    public void clickBack(MouseEvent mouseEvent) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "是否返回上一级？",
                new ButtonType("确认", ButtonBar.ButtonData.YES),
                new ButtonType("取消",ButtonBar.ButtonData.NO));
        Optional<ButtonType> buttonType = alert.showAndWait();
        if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
            Stage stage = (Stage) root.getScene().getWindow();
            stage.close();
            Parent root = null;
            root = FXMLLoader.load(getClass().getResource("../recourse/fxml/questionType.fxml"));
            stage.setScene(new Scene(root, 600, 400));
            stage.setResizable(false);
            stage.centerOnScreen();
            stage.show();
        }
    }

    public void clickLast(MouseEvent mouseEvent) {
        if (idx == 0) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "已经是第一题！");
            alert.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                    "切换后答案将清空，是否切换到上一题？",
                    new ButtonType("确认", ButtonBar.ButtonData.YES),
                    new ButtonType("取消",ButtonBar.ButtonData.NO));
            Optional<ButtonType> buttonType = alert.showAndWait();
            if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
                idx--;
                initialize(url, resourceBundle);
            }
        }
    }

    public void clickNext(MouseEvent mouseEvent) {
        if (idx == subQuestionList.size() - 1) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "已经是最后一题！");
            alert.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                    "切换后答案将清空，是否切换到下一题？",
                    new ButtonType("确认", ButtonBar.ButtonData.YES),
                    new ButtonType("取消",ButtonBar.ButtonData.NO));
            Optional<ButtonType> buttonType = alert.showAndWait();
            if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
                idx++;
                initialize(url, resourceBundle);
            }
        }
    }

    public void clickSubmit(MouseEvent mouseEvent) throws SQLException {
        SubmitDaoSocketImpl submitDaoSocket = new SubmitDaoSocketImpl();
        if (submitDaoSocket.submitSubjective(LoginController.customer.getUsername(),
                subQuestionList.get(idx).getId(),content.getText())) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "提交成功！");
            alert.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "提交失败，请重新提交！");
            alert.show();
        }
    }
}
