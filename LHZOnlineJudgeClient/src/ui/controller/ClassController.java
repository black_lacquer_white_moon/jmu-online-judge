package ui.controller;

import dao.impl.ClassesRepositoryDaoJDBCImpl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class ClassController implements Initializable {
    public static String className;
    @FXML
    private TableView<Classes> classTable;
    @FXML
    private TableColumn<Classes, String> tableColumn;
    @FXML
    private AnchorPane root;
    public static Classes clazz;
    final Integer COUNT = 2;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Classes> cellData = FXCollections.observableArrayList();
        List<String> list = (new ClassesRepositoryDaoJDBCImpl()).searchByUsername(LoginController.customer.getUsername());
        tableColumn.setCellValueFactory(new PropertyValueFactory<Classes, String>("className"));
        for (String str : list) {
            Classes classes = new Classes();
            classes.className = str;
            cellData.add(classes);
        }

        classTable.setItems(cellData);


        classTable.setRowFactory( tv -> {
            TableRow<Classes> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == COUNT && (! row.isEmpty()) ) {
                    className = row.getItem().className;
                    Stage stage = (Stage) root.getScene().getWindow();
                    Parent root = null;
                    try {
                        root = FXMLLoader.load(getClass().getResource("../recourse/fxml/classProblemSet.fxml"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    assert root != null;
                    stage.setScene(new Scene(root, 600, 400));
                    stage.setResizable(false);
                    stage.centerOnScreen();
                    stage.show();
                }
            });
            return row ;
        });
    }


    public void clickBack(MouseEvent mouseEvent) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "是否返回上一级？",
                new ButtonType("确认", ButtonBar.ButtonData.YES),
                new ButtonType("取消",ButtonBar.ButtonData.NO));
        Optional<ButtonType> buttonType = alert.showAndWait();
        if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
            Stage stage = (Stage) root.getScene().getWindow();
            //stage.close();
            Parent root = FXMLLoader.load(getClass().
                    getResource("../recourse/fxml/stuMenu.fxml"));
            stage.setScene(new Scene(root, 447, 466));
            stage.centerOnScreen();
            stage.show();
        }
    }

    public static class Classes {
        String className;

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }
    }

}


