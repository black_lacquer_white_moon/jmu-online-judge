package ui.controller;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ShowToMessageController implements Initializable {
    public Button back;
    public TextArea messageText;
    public Label addressee;
    public Label time;
    public Label title;
    public AnchorPane root;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        messageText.setWrapText(true);
        title.setText(EmailController.toMessage.getTitle());
        addressee.setText(EmailController.toMessage.getUser());
        time.setText(EmailController.toMessage.getTime().toString());
        messageText.setText(EmailController.toMessage.getText());
    }

    public void clickBack(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) root.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("../recourse/fxml/email.fxml"));
        stage.setScene(new Scene(root, 600, 400));
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }

    public Button getBack() {
        return back;
    }

    public void setBack(Button back) {
        this.back = back;
    }

    public TextArea getMessageText() {
        return messageText;
    }

    public void setMessageText(TextArea messageText) {
        this.messageText = messageText;
    }

    public Label getAddressee() {
        return addressee;
    }

    public void setAddressee(Label addressee) {
        this.addressee = addressee;
    }

    public Label getTime() {
        return time;
    }

    public void setTime(Label time) {
        this.time = time;
    }

    public Label getTitle() {
        return title;
    }

    public void setTitle(Label title) {
        this.title = title;
    }
}
