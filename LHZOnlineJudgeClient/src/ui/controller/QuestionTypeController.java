package ui.controller;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class QuestionTypeController implements Initializable {
    public Button back;
    public Button choiceButton;
    public Button programmingButton;
    public Button subjectButton;
    public Button judgmentButton;
    public AnchorPane root;
    public Label setName;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setName.setText(ClassProblemSetController.questionSetName);
    }

    public void clickBack(MouseEvent mouseEvent) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "是否返回上一级？",
                new ButtonType("确认", ButtonBar.ButtonData.YES),
                new ButtonType("取消",ButtonBar.ButtonData.NO));
        Optional<ButtonType> buttonType = alert.showAndWait();
        if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
            Stage stage = (Stage) root.getScene().getWindow();
            Parent root = FXMLLoader.load(getClass().
                    getResource("../recourse/fxml/classProblemSet.fxml"));
            stage.setScene(new Scene(root, 600, 400));
            stage.centerOnScreen();
            stage.show();
        }
    }

    public void clickChoice(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) root.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().
                getResource("../recourse/fxml/choiceQuestion.fxml"));
        stage.setScene(new Scene(root, 732, 514));
        stage.centerOnScreen();
        stage.show();
    }

    public void clickJudgment(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) root.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().
                getResource("../recourse/fxml/judgmentQuestion.fxml"));
        stage.setScene(new Scene(root, 600, 400));
        stage.centerOnScreen();
        stage.show();
    }

    public void clickProgramming(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) root.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().
                getResource("../recourse/fxml/programmingQuestion.fxml"));
        stage.setScene(new Scene(root, 806, 749));
        stage.centerOnScreen();
        stage.show();
    }

    public void clickSubjective(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) root.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().
                getResource("../recourse/fxml/subjectiveQuestion.fxml"));
        stage.setScene(new Scene(root, 600, 400));
        stage.centerOnScreen();
        stage.show();
    }
}
