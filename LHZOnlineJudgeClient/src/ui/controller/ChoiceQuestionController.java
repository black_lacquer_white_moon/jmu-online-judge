package ui.controller;

import dao.impl.ChoiceDaoJDBCImpl;
import dao.impl.ExerciseCollectionsDaoJDBCImpl;
import dao.impl.SubmitDaoSocketImpl;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.ChoiceQuestion;
import model.ExerciseCollections;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * @author 01
 * @version 1.0
 */
public class ChoiceQuestionController implements Initializable {
    public AnchorPane root;
    public TableView<Choice> choiceTable;
    public TableColumn<Choice, String> stemColumn;
    public TableColumn<Choice, RadioButton> optionA;
    public TableColumn<Choice, RadioButton> optionB;
    public TableColumn<Choice, RadioButton> optionC;
    public TableColumn<Choice, RadioButton> optionD;
    List<ChoiceQuestion> choiceQuestionList;
    Map<ChoiceQuestion, Choice> map = new LinkedHashMap<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Choice> cellData = FXCollections.observableArrayList();
        ExerciseCollections ecByName = new ExerciseCollections();
        ecByName.setName(ClassProblemSetController.questionSetName);
        (new ExerciseCollectionsDaoJDBCImpl()).getData(ecByName);
        choiceQuestionList = (new ChoiceDaoJDBCImpl()).getData(ecByName.getChoiceQuestionsId());
        stemColumn.setCellValueFactory(new PropertyValueFactory<Choice, String>("stem"));
        optionA.setCellValueFactory(new PropertyValueFactory<Choice, RadioButton>("A"));
        optionB.setCellValueFactory(new PropertyValueFactory<Choice, RadioButton>("B"));
        optionC.setCellValueFactory(new PropertyValueFactory<Choice, RadioButton>("C"));
        optionD.setCellValueFactory(new PropertyValueFactory<Choice, RadioButton>("D"));
        for (ChoiceQuestion choice : choiceQuestionList) {
            Choice choice1 = new Choice();
            choice1.setStem(choice.getText());
            List<String> list = choice.getOptions();
            choice1.a.setText(list.get(0));
            choice1.b.setText(list.get(1));
            choice1.c.setText(list.get(2));
            choice1.d.setText(list.get(3));
            map.put(choice, choice1);
            cellData.add(choice1);
        }
        choiceTable.setItems(cellData);
        choiceTable.getColumns().addListener(new ListChangeListener() {
            boolean isTurnback = false;

            @Override
            public void onChanged(Change c) {

                if (!isTurnback) {
                    while (c.next()) {
                        if (!c.wasPermutated() && !c.wasUpdated()) {
                            isTurnback = true;
                            choiceTable.getColumns().setAll(c.getRemoved());
                        }
                    }
                }
                else {
                    isTurnback = false;
                }
            }
        });

    }

    public void clickBack(MouseEvent mouseEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../recourse/fxml/questionType.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.setScene(new Scene(root, 600, 400));
        stage.setResizable(false);
        stage.centerOnScreen();
        stage.show();
    }

    public void clickSubmit(MouseEvent mouseEvent) {
        int count = 0;
        String answer = "";
        String flag = "提交成功";
        for (Map.Entry<ChoiceQuestion, Choice> map1 : map.entrySet()) {
            count++;
            if (count < map.size()) {
                if (map1.getValue().getA().selectedProperty().getValue()) {
                    answer += "1";
                }
                if (map1.getValue().getB().selectedProperty().getValue()){
                    answer += "2";
                }
                if (map1.getValue().getC().selectedProperty().getValue()) {
                    answer += "3";
                }
                if (map1.getValue().getD().selectedProperty().getValue()) {
                    answer += "4";
                }
                answer += ",";
            } else {
                if (map1.getValue().getA().selectedProperty().getValue()) {
                    answer += "1";
                }
                if (map1.getValue().getB().selectedProperty().getValue()){
                    answer += "2";
                }
                if (map1.getValue().getC().selectedProperty().getValue()) {
                    answer += "3";
                }
                if (map1.getValue().getD().selectedProperty().getValue()) {
                    answer += "4";
                }
            }
        }
        SubmitDaoSocketImpl submitDaoSocket = new SubmitDaoSocketImpl();
        System.out.println(LoginController.customer.getUsername());
        System.out.println(ClassController.className);
        System.out.println(ClassProblemSetController.exerciseCollection.getId());
        System.out.println(answer);
        if (flag.equals(submitDaoSocket.submitChoice(LoginController.customer.getUsername(), ClassController.className,
                ClassProblemSetController.exerciseCollection.getId(), answer))) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "提交成功！");
            alert.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "提交失败，请重新提交！");
            alert.show();
        }
    }

    public static class Choice {
        private String stem;
        private RadioButton a;
        private RadioButton b;
        private RadioButton c;
        private RadioButton d;

        public Choice() {
            a = new RadioButton();
            b = new RadioButton();
            c = new RadioButton();
            d = new RadioButton();
        }
        public String getStem() {
            return stem;
        }

        public void setStem(String stem) {
            this.stem = stem;
        }

        public RadioButton getA() {
            return a;
        }

        public void setA(String option) {
            a.setText(option);
        }

        public RadioButton getB() {
            return b;
        }

        public void setB(String option) {
            b.setText(option);
        }

        public RadioButton getC() {
            return c;
        }

        public void setC(String option) {
            c.setText(option);
        }

        public RadioButton getD() {
            return d;
        }

        public void setD(String option) {
            d.setText(option);
        }
    }
}
