package ui.controller;

import dao.impl.ClassesRepositoryDaoJDBCImpl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class JoinClassController implements Initializable {
    public String clazz;
    public TextField searchField;
    public Button searchButton;
    public Button back;
    public TableView<Classes> unJoinedClassTable;
    public TableColumn<Classes, String> unJoinedClass;
    @FXML
    private AnchorPane root;
    final Integer COUNT = 2;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Classes> cellData = FXCollections.observableArrayList();
        List<String> list = (new ClassesRepositoryDaoJDBCImpl()).searchUnjoinedClass(LoginController.customer.getUsername());
        unJoinedClass.setCellValueFactory(new PropertyValueFactory<Classes, String>("className"));
        for (String str : list) {
            Classes classes = new Classes();
            classes.className = str;
            cellData.add(classes);
        }

        unJoinedClassTable.setItems(cellData);


        unJoinedClassTable.setRowFactory( tv -> {
            TableRow<Classes> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == COUNT && (! row.isEmpty()) ) {
                    clazz = row.getItem().className;
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "是否加入"+clazz+"？",
                            new ButtonType("确认", ButtonBar.ButtonData.YES),
                            new ButtonType("取消",ButtonBar.ButtonData.NO));
                    Optional<ButtonType> buttonType = alert.showAndWait();
                    if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
                        if ((new ClassesRepositoryDaoJDBCImpl()).storeData(LoginController.customer.getUsername(),
                                LoginController.customer.getType(), clazz)) {
                            Alert alert1 = new Alert(Alert.AlertType.INFORMATION,"已加入"+clazz);
                            alert1.show();
                            initialize(location,resources);
                        } else {
                            Alert alert1 = new Alert(Alert.AlertType.INFORMATION,"加入"+clazz+"失败，请重新加入");
                            alert1.show();
                        }
                    }
                }
            });
            return row ;
        });
    }

    public void clickBack(MouseEvent mouseEvent) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "是否返回上一级？",
                new ButtonType("确认", ButtonBar.ButtonData.YES),
                new ButtonType("取消", ButtonBar.ButtonData.NO));
        Optional<ButtonType> buttonType = alert.showAndWait();
        if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
            Stage stage = (Stage) root.getScene().getWindow();
            stage.close();
            stage.centerOnScreen();
            Parent root = FXMLLoader.load(getClass().
                    getResource("../recourse/fxml/stuMenu.fxml"));
            stage.setScene(new Scene(root, 447, 466));
            stage.centerOnScreen();
            stage.show();
        }
    }

    public void clickSearch(MouseEvent mouseEvent) {

    }

    public static class Classes {
        String className;

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }
    }

}
