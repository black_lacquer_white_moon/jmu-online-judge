package ui.controller;

import dao.impl.ExerciseCollectionsDaoJDBCImpl;
import dao.impl.StateCollectionDaoJdbcImpl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.ExerciseCollections;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * @author 吴海波
 * @version 1.0
 */
public class ClassProblemSetController implements Initializable {
    public Label className;
    public ListView<String> doneList;
    public ListView<String> undoList;
    public AnchorPane root;
    int count = 0;
    public static String questionSetName;
    public static ExerciseCollections exerciseCollection;
    static List<ExerciseCollections> ecList = new ArrayList<>();
    final Integer COUNT = 2;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        className.setText(ClassController.className);
        try {
            ExerciseCollections doneEc = new ExerciseCollections();
            ObservableList<String> doneObservableArray = FXCollections.observableArrayList();
            List<Integer> doneIdList = (new StateCollectionDaoJdbcImpl()).getCcompleteCollections(LoginController.customer.getUsername());
            for (Integer idx : doneIdList) {
                doneEc.setId(idx);
                (new ExerciseCollectionsDaoJDBCImpl()).getData(doneEc);
                doneObservableArray.add(doneEc.getName());
                ecList.add(doneEc);
            }
            doneList.setItems(doneObservableArray);
            doneList.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    count++;
                    if (count == COUNT) {
                        if (questionSetName.equals(doneList.getSelectionModel().selectedItemProperty().getValue())) {
                            for (ExerciseCollections e : ecList) {
                                if (e.getName().equals(questionSetName)) {
                                    exerciseCollection = e;
                                    break;
                                }
                            }
                            Stage stage = (Stage) root.getScene().getWindow();
                            Parent root = null;
                            try {
                                root = FXMLLoader.load(getClass().getResource("../recourse/fxml/questionType.fxml"));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            stage.setScene(new Scene(root, 600, 400));
                            stage.centerOnScreen();
                            stage.show();
                        } else {
                            count = 0;
                        }
                    } else {
                        questionSetName = doneList.getSelectionModel().selectedItemProperty().getValue();
                    }
                }
            });
            ExerciseCollections undoEc = new ExerciseCollections();
            ObservableList<String> undoObservableArray = FXCollections.observableArrayList();
            List<Integer> undoIdList = (new StateCollectionDaoJdbcImpl()).getIncompleteCollections(LoginController.customer.getUsername());
            for (Integer idx : undoIdList) {
                undoEc.setId(idx);
                (new ExerciseCollectionsDaoJDBCImpl()).getData(undoEc);
                undoObservableArray.add(undoEc.getName());
                ecList.add(undoEc);
            }
            undoList.setItems(undoObservableArray);
            undoList.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    count++;
                    if (count == COUNT) {
                        if (questionSetName.equals(undoList.getSelectionModel().selectedItemProperty().getValue())) {
                            for (ExerciseCollections e : ecList) {
                                if (e.getName().equals(questionSetName)) {
                                    exerciseCollection = e;
                                    break;
                                }
                            }
                            Stage stage = (Stage) root.getScene().getWindow();
                            Parent root = null;
                            try {
                                root = FXMLLoader.load(getClass().getResource("../recourse/fxml/questionType.fxml"));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            stage.setScene(new Scene(root, 600, 400));
                            stage.setResizable(false);
                            stage.centerOnScreen();
                            stage.show();
                        } else {
                            count = 0;
                        }
                    } else {
                        questionSetName = undoList.getSelectionModel().selectedItemProperty().getValue();
                    }
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void clickBack(MouseEvent mouseEvent) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "是否返回上一级？",
                new ButtonType("确认", ButtonBar.ButtonData.YES),
                new ButtonType("取消", ButtonBar.ButtonData.NO));
        Optional<ButtonType> buttonType = alert.showAndWait();
        if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
            Stage stage = (Stage) root.getScene().getWindow();
            Parent root = FXMLLoader.load(getClass().
                    getResource("../recourse/fxml/class.fxml"));
            stage.setScene(new Scene(root, 600, 400));
            stage.centerOnScreen();
            stage.show();
        }
    }
}
