package ui.controller;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class showFromMessageController implements Initializable {
    public Label title;
    public Label addresser;
    public Button back;
    public Label time;
    public AnchorPane root;
    public TextArea messageText;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        messageText.setWrapText(true);
        title.setText(EmailController.fromMessage.getTitle());
        addresser.setText(EmailController.fromMessage.getUser());
        time.setText(EmailController.fromMessage.getTime().toString());
        messageText.setText(EmailController.fromMessage.getText());
    }

    public void clickBack(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) root.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("../recourse/fxml/email.fxml"));
        stage.setScene(new Scene(root, 600, 400));
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }

    public Label getTitle() {
        return title;
    }

    public void setTitle(Label title) {
        this.title = title;
    }

    public Label getAddresser() {
        return addresser;
    }

    public void setAddresser(Label addresser) {
        this.addresser = addresser;
    }

    public Button getBack() {
        return back;
    }

    public void setBack(Button back) {
        this.back = back;
    }

    public Label getTime() {
        return time;
    }

    public void setTime(Label time) {
        this.time = time;
    }

    public AnchorPane getRoot() {
        return root;
    }

    public void setRoot(AnchorPane root) {
        this.root = root;
    }
}
