package ui.controller;

import dao.impl.ExerciseCollectionsDaoJDBCImpl;
import dao.impl.JudgmentDaoJDBCImpl;
import dao.impl.SubmitDaoSocketImpl;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.ExerciseCollections;
import model.JudgmentQuestion;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class JudgmentQuestionController implements Initializable {
    public AnchorPane root;
    public TableView<Judgment> judgmentTable;
    public TableColumn<Judgment, String> judgmentColumn;
    public TableColumn<Judgment, RadioButton> right;
    public TableColumn<Judgment, RadioButton> wrong;
    List<JudgmentQuestion> judgmentQuestions;
    Map<JudgmentQuestion, Judgment> map = new LinkedHashMap<>();
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Judgment> cellData = FXCollections.observableArrayList();
        ExerciseCollections ecByName = new ExerciseCollections();
        ecByName.setName(ClassProblemSetController.questionSetName);
        (new ExerciseCollectionsDaoJDBCImpl()).getData(ecByName);
        judgmentQuestions = (new JudgmentDaoJDBCImpl()).getData(ecByName.getJudgmentQuestionsId());
        judgmentColumn.setCellValueFactory(new PropertyValueFactory<Judgment, String>("text"));
        right.setCellValueFactory(new PropertyValueFactory<Judgment, RadioButton>("right"));
        wrong.setCellValueFactory(new PropertyValueFactory<Judgment, RadioButton>("wrong"));
        for (JudgmentQuestion judge : judgmentQuestions) {
            Judgment judgment = new Judgment();
            judgment.setText(judge.getText());
            map.put(judge, judgment);
            cellData.add(judgment);
            judgment.right.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    judgment.wrong.selectedProperty().setValue(false);
                }
            });
            judgment.wrong.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    judgment.right.selectedProperty().setValue(false);
                }
            });
        }
        judgmentTable.setItems(cellData);
        judgmentTable.getColumns().addListener(new ListChangeListener() {
            boolean isTurnBack = false;

            @Override
            public void onChanged(Change c) {

                if (!isTurnBack) {
                    while (c.next()) {
                        if (!c.wasPermutated() && !c.wasUpdated()) {
                            isTurnBack = true;
                            judgmentTable.getColumns().setAll(c.getRemoved());
                        }
                    }
                }
                else {
                    isTurnBack = false;
                }
            }
        });

    }

    public void clickBack(MouseEvent mouseEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        stage.close();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../recourse/fxml/questionType.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.setScene(new Scene(root, 600, 400));
        stage.setResizable(false);
        stage.centerOnScreen();
        stage.show();
    }

    public void clickSubmit(MouseEvent mouseEvent) {
        int count = 0;
        String answer = "";
        String flag = "提交成功";
        for (Map.Entry<JudgmentQuestion, Judgment> map1 : map.entrySet()) {
            count++;
            if (count < map.size()) {
                if (map1.getValue().getRight().selectedProperty().getValue()) {
                    answer += "1,";
                } else {
                    answer += "0,";
                }
            } else {
                if (map1.getValue().getRight().selectedProperty().getValue()) {
                    answer += "1";
                } else {
                    answer += "0";
                }
            }
        }
        System.out.println(answer);
        SubmitDaoSocketImpl submitDaoSocket = new SubmitDaoSocketImpl();

        if (submitDaoSocket.submitJudgment(LoginController.customer.getUsername(),ClassController.className,
                ClassProblemSetController.exerciseCollection.getId(), answer).equals(flag)) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "提交成功！");
            alert.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "提交失败，请重新提交！");
            alert.show();
        }
    }

    public static class Judgment{
        int id;
        int choice;
        String text;
        RadioButton right;
        RadioButton wrong;

        public Judgment() {
            right = new RadioButton();
            wrong = new RadioButton();
        }
        public RadioButton getRight() {
            return right;
        }

        public void setRight(RadioButton right) {
            this.right = right;
        }

        public RadioButton getWrong() {
            return wrong;
        }

        public void setWrong(RadioButton wrong) {
            this.wrong = wrong;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

    }
}
