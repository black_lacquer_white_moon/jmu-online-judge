package ui.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class StuSettingController {
    @FXML
    private AnchorPane root;

    public void backClick(MouseEvent mouseEvent) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "是否返回上一级？",
                new ButtonType("确认", ButtonBar.ButtonData.YES),
                new ButtonType("取消",ButtonBar.ButtonData.NO));
        Optional<ButtonType> buttonType = alert.showAndWait();
        if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
            Stage stage = (Stage) root.getScene().getWindow();
            //stage.close();
            Parent root = FXMLLoader.load(getClass().
                    getResource("../recourse/fxml/stuMenu.fxml"));
            stage.setScene(new Scene(root, 447, 466));
            stage.setResizable(false);
            stage.centerOnScreen();
            stage.show();
        }
    }

    public void clickChange(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) root.getScene().getWindow();
        //stage.close();
        Parent root = FXMLLoader.load(getClass().
                getResource("../recourse/fxml/changePassword.fxml"));
        stage.setScene(new Scene(root, 351, 248));
        stage.setResizable(false);
        stage.centerOnScreen();
        stage.show();
    }

    public void clickCancellation(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) root.getScene().getWindow();
        //stage.close();
        Parent root = FXMLLoader.load(getClass().
                getResource("../recourse/fxml/cancelAccount.fxml"));
        stage.setScene(new Scene(root, 283, 176));
        stage.setResizable(false);
        stage.centerOnScreen();
        stage.show();
    }
}
