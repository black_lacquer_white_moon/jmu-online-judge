package ui.controller;

import dao.impl.UserDaoImpl;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class LogonController {
    public Button logon;
    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    @FXML
    private PasswordField password2;
    @FXML
    private AnchorPane root;

    public void clickLogon(MouseEvent actionEvent) throws IOException {
        if (username.getText().isEmpty() | password.getText().isEmpty() | password2.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "用户名和/或密码不能为空");
            alert.show();
        } else if (!password.getText().equals(password2.getText())) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "两次输入的密码不同");
            alert.show();
        } else if (!UserDaoImpl.registerUser(username.getText(), password.getText())){
            Alert alert = new Alert(Alert.AlertType.WARNING, "用户已存在");
            alert.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "注册成功",
                    new ButtonType("确认", ButtonBar.ButtonData.YES));
            Optional<ButtonType> buttonType = alert.showAndWait();
            if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
                Stage stage = (Stage) root.getScene().getWindow();
                stage.close();
                stage.centerOnScreen();
                Parent root = FXMLLoader.load(getClass().getResource("../recourse/fxml/login.fxml"));
                stage.setScene(new Scene(root, 328, 238));
                stage.setResizable(false);
                stage.show();
            }
        }
    }

    public void clickCancel(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) root.getScene().getWindow();
        stage.centerOnScreen();
        Parent root = FXMLLoader.load(getClass().getResource("../recourse/fxml/login.fxml"));
        stage.setScene(new Scene(root, 328, 238));
        stage.setResizable(false);
        stage.show();
    }
}
