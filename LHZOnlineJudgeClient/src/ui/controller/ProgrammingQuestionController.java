package ui.controller;

import dao.impl.*;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.ExerciseCollections;
import model.Message;
import model.ProgrammingQuestion;
import model.SubjectiveQuestion;

import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class ProgrammingQuestionController implements Initializable {
    public Button back;
    public Label title;
    public AnchorPane root;
    public Label stem;
    public Label inputExample;
    public Label outputExample;
    public TextArea programmingArea;
    int idx = 0;
    URL url;
    ResourceBundle resourceBundle;
    List<ProgrammingQuestion> programmingQuestionList;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        programmingArea.clear();
        url = location;
        resourceBundle =resources;
        programmingArea.setWrapText(true);
        ExerciseCollections ecByName = new ExerciseCollections();
        ecByName.setName(ClassProblemSetController.questionSetName);
        (new ExerciseCollectionsDaoJDBCImpl()).getData(ecByName);
        programmingQuestionList =
                (new ProgrammingDaoJDBCImpl()).getData(ecByName.getProgrammingQuestionsId());
        stem.setText(programmingQuestionList.get(idx).getText());
        title.setText(programmingQuestionList.get(idx).getTitle());
        inputExample.setText(programmingQuestionList.get(idx).getInput());
        outputExample.setText(programmingQuestionList.get(idx).getOutput());
    }

    public void clickBack(MouseEvent mouseEvent) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "未提交退出后答案不保存，是否已提交？",
                new ButtonType("确认", ButtonBar.ButtonData.YES),
                new ButtonType("取消",ButtonBar.ButtonData.NO));
        Optional<ButtonType> buttonType = alert.showAndWait();
        if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
            Stage stage = (Stage) root.getScene().getWindow();
            stage.close();
            stage.centerOnScreen();
            Parent root = FXMLLoader.load(getClass().
                    getResource("../recourse/fxml/questionType.fxml"));
            stage.setScene(new Scene(root, 600, 400));
            stage.setResizable(false);
            stage.show();
        }
    }

    public void clickLast(MouseEvent mouseEvent) {
        if (idx == 0) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "已经是第一题！");
            alert.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                    "切换后答案将清空，是否切换到上一题？",
                    new ButtonType("确认", ButtonBar.ButtonData.YES),
                    new ButtonType("取消",ButtonBar.ButtonData.NO));
            Optional<ButtonType> buttonType = alert.showAndWait();
            if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
                idx--;
                initialize(url, resourceBundle);
            }
        }
    }

    public void clickNext(MouseEvent mouseEvent) {
        if (idx == programmingQuestionList.size() - 1) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "已经是最后一题！");
            alert.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                    "切换后答案将清空，是否切换到下一题？",
                    new ButtonType("确认", ButtonBar.ButtonData.YES),
                    new ButtonType("取消",ButtonBar.ButtonData.NO));
            Optional<ButtonType> buttonType = alert.showAndWait();
            if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
                idx++;
                initialize(url, resourceBundle);
            }
        }
    }

    public void clickSubmit(MouseEvent mouseEvent) {
        String flag = "提交成功";
        SubmitDaoSocketImpl submitDaoSocket = new SubmitDaoSocketImpl();
        String result = submitDaoSocket.submitProgram(LoginController.customer.getUsername(),
                ClassController.className, ClassProblemSetController.exerciseCollection.getId(),
                programmingQuestionList.get(idx).getId(), programmingArea.getText());
        Alert alert;
        if (result.contains(flag)) {
            alert = new Alert(Alert.AlertType.INFORMATION, result);
        } else {
            alert = new Alert(Alert.AlertType.INFORMATION, "提交失败，请重新提交！");
        }
        alert.show();
    }
}
