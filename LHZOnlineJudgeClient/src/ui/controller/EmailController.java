package ui.controller;

import dao.impl.MessagesDaoJdbcImpl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.DataFormat;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Message;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class EmailController implements Initializable {
    @FXML
    public Button sendButton;
    @FXML
    public TextField titleField;
    @FXML
    public TextField receiver;
    @FXML
    private TableView<Message> toTable;
    @FXML
    private TableView<Message> fromTable;
    @FXML
    private TableColumn<Message, Timestamp> receiveTime;
    @FXML
    private TableColumn<Message, String> addresserTitle;
    @FXML
    private TableColumn<Message, String> addresseeTitle;
    @FXML
    private TextArea emailText;
    @FXML
    public TableColumn<Message, Timestamp> sendTime;
    @FXML
    private TableColumn<Message, String> addresser;//发件人
    @FXML
    private TableColumn<Message, String> addressee;//收件人
    @FXML
    private AnchorPane email;
    public static Message fromMessage;
    public static Message toMessage;
    final Integer COUNT = 2;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        emailText.setWrapText(true);
        //收件箱初始化
        try {
            ObservableList<Message> cellData = FXCollections.observableArrayList();
            List<Message> fromMessageList;
            fromMessageList = MessagesDaoJdbcImpl.getReceivedEmails(LoginController.customer.getUsername());
            addresser.setCellValueFactory(new PropertyValueFactory<Message, String>("addresser"));
            receiveTime.setCellValueFactory(new PropertyValueFactory<Message, Timestamp>("time"));
            addresserTitle.setCellValueFactory(new PropertyValueFactory<Message, String>("title"));

            for (Message m : fromMessageList) {
                Message message = m;
                cellData.add(message);
            }


            fromTable.setItems(cellData);
            fromTable.setRowFactory( tv -> {
                TableRow<Message> row = new TableRow<>();
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == COUNT && (! row.isEmpty()) ) {
                       int idx = fromMessageList.indexOf(row.getItem());
                        System.out.println(idx);
                        String title = row.getItem().getTitle();
                        fromMessage = fromMessageList.get(idx);
                        Stage stage = (Stage) email.getScene().getWindow();
                        Parent root = null;
                        try {
                            root = FXMLLoader.load(getClass().getResource("../recourse/fxml/showFromMessage.fxml"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        stage.setScene(new Scene(root, 600, 400));
                        stage.setResizable(false);
                        stage.centerOnScreen();
                        stage.show();
                    }
                });
                return row ;
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            ObservableList<Message> cellData = FXCollections.observableArrayList();
            List<Message> toMessageList;
            toMessageList = MessagesDaoJdbcImpl.getSendedEmails(LoginController.customer.getUsername());
            addressee.setCellValueFactory(new PropertyValueFactory<Message, String>("user"));
            sendTime.setCellValueFactory(new PropertyValueFactory<Message, Timestamp>("time"));
            addresseeTitle.setCellValueFactory(new PropertyValueFactory<Message, String>("title"));

            for (Message m : toMessageList) {
                Message message = new Message();
                message = m;
                cellData.add(message);
            }

            toTable.setItems(cellData);
            toTable.setRowFactory( tv -> {
                TableRow<Message> row = new TableRow<>();
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == COUNT && (! row.isEmpty()) ) {
                        int idx = toMessageList.indexOf(row.getItem());
                        toMessage = toMessageList.get(idx);
                        Stage stage = (Stage) email.getScene().getWindow();
                        Parent root = null;
                        try {
                            root = FXMLLoader.load(getClass().getResource("../recourse/fxml/showToMessage.fxml"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        stage.setScene(new Scene(root, 600, 400));
                        stage.setResizable(false);
                        stage.centerOnScreen();
                        stage.show();
                    }
                });
                return row ;
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void clickBack(MouseEvent mouseEvent) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "是否返回上一级？",
                new ButtonType("确认",ButtonBar.ButtonData.YES),
                new ButtonType("取消",ButtonBar.ButtonData.NO));
        Optional<ButtonType> buttonType = alert.showAndWait();
        if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
            Stage stage = (Stage) email.getScene().getWindow();
            //stage.close();
            Parent root = FXMLLoader.load(getClass().
                    getResource("../recourse/fxml/stuMenu.fxml"));
            stage.setScene(new Scene(root, 447, 466));
            stage.setResizable(false);
            stage.centerOnScreen();
            stage.show();
        }
    }

    public void clickSend(MouseEvent mouseEvent) throws SQLException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                "是否发送给"+receiver.getText()+"？",
                new ButtonType("确认", ButtonBar.ButtonData.YES),
                new ButtonType("取消",ButtonBar.ButtonData.NO));
        Optional<ButtonType> buttonType = alert.showAndWait();
        if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)) {
            Message mess = new Message();
            mess.setText(emailText.getText());
            mess.setTime(new Timestamp(System.currentTimeMillis()));
            mess.setUser(receiver.getText());
            mess.setAddresser(LoginController.customer.getUsername());
            mess.setTitle(titleField.getText());
            if (MessagesDaoJdbcImpl.sendEmail(mess)) {
                Alert alert1 = new Alert(Alert.AlertType.INFORMATION, "发送成功");
                alert1.show();
            } else {
                Alert alert1 = new Alert(Alert.AlertType.INFORMATION, "发送失败");
                alert1.show();
            }
        }
    }
}
