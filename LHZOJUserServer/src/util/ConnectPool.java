package util;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;

/**
* ConnectPool类为自建的可扩容的简易数据库连接池
* @author 林智凯
* @version 1.0
*/
public class ConnectPool {
	
	private List<Connection> pool = new ArrayList<Connection>();
	private Integer maxSize;
	
	/**
	 * 数据库连接池的构造方法，预先分配5个Connection对象
	 */
	public  ConnectPool() throws SQLException {
		Integer num = 5;
		this.maxSize = num;
		for (int i = 0; i < num; i++) {
			Connection conn = MysqlConnect.connectDatabase();
			this.pool.add(conn);
		}
	}
	
	/**
	 * 向数据库连接池获取连接资源，返回一个 Connection 对象
	 * @return Connection 对象
	 */
	public Connection getConnection() throws SQLException {
		//检查是否还有Connection对象可以分配
		if(pool.size() == 0){
			//Connection对象不够用，先扩容
			for (int i = 0; i < this.maxSize; i++) {
				Connection conn = MysqlConnect.connectDatabase();
				pool.add(conn);
			}
			//2倍扩容
			this.maxSize *= 2;
		}
		//弹出第一个Connection对象返回
		Connection conn = pool.remove(0);
		return conn;
	}
	
	/**
	 * 回收 Connection 返回数据库连接池中。
	 * @param conn
	 */
	public void recoveryConnection(Connection conn){
		pool.add(conn);
	}

}
