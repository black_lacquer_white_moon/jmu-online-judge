package dao.impl;

import java.sql.*;

import dao.*;
import util.MysqlConnect;
import controller.Server;

/**
* 本类基于MySQL实现UserRepositoryDAO接口
* @author 林智凯
* @version 1.1
*/
public class UserRepositoryDaoJdbcImpl implements UserRepositoryDAO {
	
	/**
	   * 无参构造方法，支持DAO模式
	*/
	public UserRepositoryDaoJdbcImpl() {	}
	
	/**
	   * 这个方法将基于select查找用户名是否已存在，用户注册时用
	   * @param username 被查找的用户名
	   * @return true为用户名不存在，false为用户名已存在
	 * @throws SQLException 数据库异常
	   */
	public boolean selectUsername(String username) throws SQLException {
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;   
		//创建 ResultSet 结果集对象
		ResultSet rs = null;	
		boolean flag = false;
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();  
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlSelect = "SELECT username FROM users WHERE binary username = '%s';";
			//查询用户名是否存在，是则重复，无法注册
			rs = statement.executeQuery(String.format(sqlSelect, username));
			rs.last();
			if(rs.getRow() > 0) {
				//用户名重复
				flag = false;    
			}
			else {
				//用户名可用
			    flag = true;    
			}
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
		
		return flag;
	}
	
	/**
	   * 这个方法将基于select核对用户名和密码是否存在且匹配，用户登录和其他增删改操作时使用
	   * @param username 用户名
	   * @param password 密码
	   * @return true为用户名和密码存在且匹配，false为用户名或密码错误
	 * @throws SQLException 数据库异常
	   */
	public boolean checkUser(String username, String password) throws SQLException {
		Connection conn = null;    
		//创建 Connection 数据库连接对象
		Statement statement = null;    
		//创建静态 SQL 语句 Statement 对象
		ResultSet rs = null;	
		//创建 ResultSet 结果集对象
		boolean flag = false;
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();  
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sql = "SELECT username,password FROM users WHERE binary username = '%s' AND password = '%s';";
			rs = statement.executeQuery(String.format(sql, username, password));
			rs.last();
			if(rs.getRow() == 1) {
				//找到匹配的条目
				flag = true;    
			}
			else {
				 //用户名或密码错误
				flag = false;   
			}
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
		
		return flag;
	}
	
	/**
	   * 这个方法将基于insert向数据库插入一个uesr记录，注册操作时用，调用该方法前应使用selectUsername()方法检查
	   * @param username 用户名
	   * @param password 密码
	   * @return true为记录插入成功，false为插入失败
	 * @throws SQLException 数据库异常
	   */
	public boolean insertUser(String username, String password) throws SQLException {
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;    
		boolean flag = false;
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();  
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlInsert = " INSERT INTO users(username, password) values('%s','%s'); ";
			//判断插入是否成功
			if(statement.executeUpdate(String.format(sqlInsert, username, password)) != 0) {
				flag = true;
			}
			else {
				flag = false;
			}
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
		
		return flag;
	}
	
	/**
	   * 这个方法将基于delete删除一条记录，注销用户时用，调用该方法前应使用checkUser()方法检查
	   * @param username 用户名
	   * @return true为删除成功，false删除失败
	 * @throws SQLException 数据库异常
	   */
	public boolean deleteUser(String username) throws SQLException {
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;    
		boolean flag = false;
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();  
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlDelete = "DELETE FROM users WHERE username = '%s'";
			//判断是否能删除成功
			if(statement.executeUpdate(String.format(sqlDelete, username)) != 0) {
				flag = true;
			}
			else { 
				flag = false;
			}
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
		
		return flag;
	}
	
	/**
	   * 这个方法将基于update向更新一个uesr的password字段，改密码操作时用，调用该方法前应使用checkUser()方法检查
	   * @param username 用户名
	   * @param newPassword 新密码，用于替换原有的条目
	   * @return true为更换成功，false更换失败
	 * @throws SQLException 数据库异常
	   */
	public boolean updateUserPasswd(String username, String newPassword) throws SQLException {
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;    
		boolean flag = false;
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();  
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlUpdate = "UPDATE users SET password='%s' WHERE username = '%s'";
			//判断是否能更新成功
			if(statement.executeUpdate(String.format(sqlUpdate, newPassword, username)) != 0) {
				flag = true;
			}
			else { 
				flag = false;
			}
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
			//关闭所有资源
		}finally{    
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
		
		return flag;
	}
	
}
