package dao;

import java.sql.SQLException;

/**
* UserRepositoryDAO 接口指定了与数据库的User交互行为
* @author 林智凯
* @version 1.1
*/
public interface UserRepositoryDAO {
	      
	/**
	   * 查找用户名是否已存在，用户注册时用
	   * @param username 被查找的用户名
	   * @return true为用户名不存在，false为用户名已存在
	 * @throws SQLException 数据库异常
	   */
	public static boolean selectUsername(String username) throws SQLException{
		return false;
	}
	
	/**
	   * 核对用户名和密码是否存在且匹配，用户登录和其他增删改操作时使用
	   * @param username 用户名
	   * @param password 密码
	   * @return true为用户名和密码存在且匹配，false为用户名或密码错误
	 * @throws SQLException 数据库异常                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
	   */
	public static boolean checkUser(String username, String password) throws SQLException{
		return false;
	}
	
	/**
	   * 向数据库插入一个uesr记录，注册操作时用，调用该方法前应使用selectUsername()方法检查
	   * @param username 用户名
	   * @param password 密码
	   * @return true为记录插入成功，false为插入失败
	 * @throws SQLException 数据库异常
	   */
	public static boolean insertUser(String username, String password) throws SQLException{
		return false;
	}
	
	/**
	   * 删除一条记录，注销用户时用，调用该方法前应使用checkUser()方法检查
	   * @param username 用户名
	   * @return true为删除成功，false删除失败
	 * @throws SQLException 数据库异常
	   */
	public static boolean deleteUser(String username) throws SQLException{
		return false;
	}
	
	/**
	   * 更新一个uesr的password字段，改密码操作时用，调用该方法前应使用checkUser()方法检查
	   * @param username 用户名
	   * @param newPassword 新密码，用于替换原有的条目
	   * @return true为更换成功，false更换失败
	 * @throws SQLException 数据库异常
	   */
	public static boolean updateUserPasswd(String username, String newPassword) throws SQLException{
		return false;
	}
}
