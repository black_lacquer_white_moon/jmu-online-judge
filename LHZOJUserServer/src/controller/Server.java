package controller;

import java.net.*;
import java.sql.SQLException;
import java.util.Base64;

import model.Response;
import util.ConnectPool;

import java.io.*;

import org.apache.log4j.Logger;

/**
* 本类为服务器，对于客户端的请求进行响应响应的方式为true、false
* @author 林智凯
* @version 1.0
*/
public class Server extends Thread
{
	
   private ServerSocket serverSocket;
   /**数据库连接池*/
   private static ConnectPool pool;
   
   public Server(int port) throws IOException
   {
      serverSocket = new ServerSocket(port);
      serverSocket.setSoTimeout(1000000);
   }
 
   @Override
   /**
	   * 这个方法将覆盖Thread的run()方法，运行服务器的套接字，响应用户的请求
	   */
   public void run()
   {
	   Logger log = Logger.getLogger(Server.class);
      while(true)
      {
         try
         {
        	log.info("等待远程连接，端口号为：" + serverSocket.getLocalPort() + "...");
            Socket server = serverSocket.accept();
            log.info("远程主机地址：" + server.getRemoteSocketAddress());
            DataInputStream in = new DataInputStream(server.getInputStream());
            //将客户端传来的密文转成明文
            String resultDecode = new String(Base64.getDecoder().decode(in.readUTF()));
            log.info("从" + server.getRemoteSocketAddress() + "接收" + resultDecode);
            
            //分割明文，执行对应的操作
            String[] resultSet = resultDecode.split(" ");
            
            boolean flag = false;
            //根据操作码，执行不同的操作
            switch(resultSet[0]) {
            case "1":{
            	//响应用户的注册请求
            	flag = Response.responseRegister(resultSet[1], resultSet[2]);
            	break;
            }
            case "2":{
            	//响应用户的登录请求
            	flag = Response.responseSignIn(resultSet[1], resultSet[2]);
            	break;
            }
            case "3":{
            	//响应用户的改密码请求
            	flag = Response.responseChange(resultSet[1], resultSet[2], resultSet[3]);
            	break;
            }
            case "4":{
            	//响应用户的注销请求
            	flag = Response.responseCancel(resultSet[1], resultSet[2]);
            	break;
            }
            default :{
            	flag = false;
            	break;
            }
            }
            
            //将响应结果发回客户端
            DataOutputStream out = new DataOutputStream(server.getOutputStream());
            if(flag == true) {
            	out.writeUTF(Base64.getEncoder().encodeToString("true".getBytes("utf-8")));
            }
            else {
            	out.writeUTF(Base64.getEncoder().encodeToString("false".getBytes("utf-8")));
            }
            log.info("向" + server.getRemoteSocketAddress() + "发送" + out);
            server.close();
         }
         
         catch(SocketTimeoutException s)
         {
        	 log.info("Socket timed out!");
            break;
         }catch(IOException e)
         {
            e.printStackTrace();
            log.error(e);
            break;
         } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error(e);
		}
      }
   }
   
   /**
	   * 数据库连接池的访问器
	   * @throws SQLException 
	   */
   public static ConnectPool getPool() {
	   return pool;
   }

   /**
	   * 数据库连接池的修改器
	   * @throws SQLException 
	   */
   public static void setPool() throws SQLException {
	   Server.pool = new ConnectPool();
   }
   
   /**
	   * 启动套接字，接收客户端的信息
	   * @throws SQLException 
	   */
   public static void main(String [] args) throws SQLException
   {
      int port = Integer.parseInt("12000");
      //初始化数据库连接池
      Server.setPool();
      try
      {
         Thread t = new Server(port);
         t.run();
      }catch(IOException e)
      {
         e.printStackTrace();
      }
   }
}