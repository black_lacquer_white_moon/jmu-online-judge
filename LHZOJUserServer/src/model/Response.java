package model;

import java.sql.SQLException;

import dao.impl.*;

/**
* 本类的方法用于执行对客户端的响应
* @author 林智凯
* @version 1.0
*/
public class Response {
	
	public static UserRepositoryDaoJdbcImpl UserRepository = new UserRepositoryDaoJdbcImpl();
			
	/**
	   * 这个方法对客户端的Register请求进行响应
	   * @param username 用户名
	   * @param password 密码
	   * @return true为成功响应 ，false为响应失败
	 * @throws SQLException 数据库异常
	   */
	public static boolean responseRegister(String username, String password) throws SQLException {
		try {
			//先使用selectUsername方法检查用户名是否已存在
			if(UserRepository.selectUsername(username) == true) {
				//把用户名和密码写入数据库
				if(UserRepository.insertUser(username, password) == true) {
					//写入成功
					return true;    
					}
				else {
					//写入失败
					return false;    
					}
				}
			else {
				//用户名已存在
				return false;    
				}
			
			} catch (SQLException e) {
				throw e;
				}
		}
	
	/**
	   * 这个方法对客户端的ChangePassword请求进行响应
	   * @param username 用户名
	   * @param password 密码
	   * @param newPassword 新密码
	   * @return true为成功响应 ，false为响应失败
	 * @throws SQLException 数据库异常
	   */
	public static boolean responseChange(String username, String password, String newPassword) throws SQLException {
		try {
			//先使用checkUser验证用户
			if(UserRepository.checkUser(username, password) == true) {
				//把新密码写入数据库
				if(UserRepository.updateUserPasswd(username, newPassword) == true) {
					//写入成功
					return true;    
					}
				else {
					//写入失败
					return false;    
					}
				}
			else {
				//验证失败
				return false;    
				}
			
			} catch (SQLException e) {
				throw e;
				}
		}
	
	/**
	   * 这个方法对客户端的logOff请求进行响应
	   * @param username 用户名
	   * @param password 密码
	   * @return true为成功响应 ，false为响应失败
	 * @throws SQLException 数据库异常
	   */
	public static boolean responseCancel(String username, String password) throws SQLException {
		try {
			//先使用checkUser验证用户
			if(UserRepository.checkUser(username, password) == true) {
				//把用户对应的条目删了
				if(UserRepository.deleteUser(username) == true) {
					 //删除成功
					return true;   
					}
				else {
					//删除失败
					return false;    
					}
				}
			else {
				//验证失败
				return false;    
				}
			
			} catch (SQLException e) {
				throw e;
				}
		}
	
	/**
	   * 这个方法对客户端的SignIn请求进行响应
	   * @param username 用户名
	   * @param password 密码
	   * @return true为成功响应 ，false为响应失败
	 * @throws SQLException 数据库异常
	   */
	public static boolean responseSignIn(String username, String password) throws SQLException {
		try {
			//使用checkUser验证用户
			if(UserRepository.checkUser(username, password) == true) {
				return true;
			}
			else {
				//验证失败
				return false;    
				}
			
			} catch (SQLException e) {
				throw e;
				}
		}
}
