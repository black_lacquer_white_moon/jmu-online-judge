﻿# Host: localhost  (Version: 5.7.26)
# Date: 2021-01-28 19:31:40
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "answer"
#

DROP TABLE IF EXISTS `answer`;
CREATE TABLE `answer` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `questiond_id` int(11) DEFAULT NULL COMMENT '题目id',
  `text` mediumtext COMMENT '用户作答情况',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='主观题的作答';

#
# Structure for table "choice"
#

DROP TABLE IF EXISTS `choice`;
CREATE TABLE `choice` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `text` mediumtext COMMENT '题干',
  `options` text COMMENT '选项',
  `answer` varchar(255) DEFAULT NULL COMMENT '答案',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='选择题';

#
# Structure for table "classes"
#

DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `type` int(1) DEFAULT '0' COMMENT '用户身份，0学生，1教师',
  `classname` varchar(255) DEFAULT NULL COMMENT '班级',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='班级信息';

#
# Structure for table "codes"
#

DROP TABLE IF EXISTS `codes`;
CREATE TABLE `codes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `questiond_id` int(11) DEFAULT NULL COMMENT '题目id',
  `code` mediumtext COMMENT '代码',
  `clean_code` int(11) DEFAULT NULL COMMENT '清洗后的代码',
  `state` int(1) DEFAULT '0' COMMENT '作答状态，0错误，1正确',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='代码';

#
# Structure for table "collections"
#

DROP TABLE IF EXISTS `collections`;
CREATE TABLE `collections` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '题目集名称',
  `choice` text COMMENT '选择题(“,”分隔)',
  `judgment` text COMMENT '判断题',
  `programming` text COMMENT '编程题',
  `subjective` text COMMENT '主观题',
  `score_choice` int(11) DEFAULT '0' COMMENT '选择题分数',
  `score_judgment` int(11) DEFAULT '0' COMMENT '判断题分数',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='题目集';

#
# Structure for table "emails"
#

DROP TABLE IF EXISTS `emails`;
CREATE TABLE `emails` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `addresser` varchar(255) DEFAULT NULL,
  `text` text,
  `sendtime` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='站内短信';

#
# Structure for table "judgment"
#

DROP TABLE IF EXISTS `judgment`;
CREATE TABLE `judgment` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `text` mediumtext COMMENT '题干',
  `answer` varchar(1) DEFAULT NULL COMMENT '答案，0错误，1正确',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='判断题';

#
# Structure for table "program"
#

DROP TABLE IF EXISTS `program`;
CREATE TABLE `program` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text COMMENT '题目名',
  `text` mediumtext COMMENT '题干',
  `input` text COMMENT '输入说明',
  `output` text COMMENT '输出说明',
  `testpoint` int(1) DEFAULT NULL COMMENT '是否有测试点，1有，0没有',
  `score` int(11) DEFAULT NULL COMMENT '得分',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='编程题';

#
# Structure for table "score"
#

DROP TABLE IF EXISTS `score`;
CREATE TABLE `score` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL COMMENT '作答学生',
  `collection_id` int(11) DEFAULT NULL COMMENT '题目集id',
  `choice` int(11) DEFAULT '-1' COMMENT '选择题',
  `judgment` int(11) DEFAULT '-1' COMMENT '判断题',
  `programming` int(11) DEFAULT '-1' COMMENT '编程题',
  `subjective` int(11) DEFAULT '-1' COMMENT '主观题',
  `class_id` varchar(11) DEFAULT NULL COMMENT '班级名',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='学生题集的分数';

#
# Structure for table "subjective"
#

DROP TABLE IF EXISTS `subjective`;
CREATE TABLE `subjective` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `text` mediumtext COMMENT '题干',
  `score` int(11) DEFAULT NULL COMMENT '得分',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='主观题';

#
# Structure for table "task"
#

DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '班级名称',
  `collection_id` int(11) DEFAULT NULL COMMENT '题目集id',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='每个班级有的题目';

#
# Structure for table "testpoint"
#

DROP TABLE IF EXISTS `testpoint`;
CREATE TABLE `testpoint` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `input` text COMMENT '输入样例',
  `output` text COMMENT '输出样例',
  `question` int(11) DEFAULT NULL COMMENT '所属题目',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='测试点';

#
# Structure for table "users"
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL COMMENT 'MD5加密后的用户名',
  `password` varchar(255) DEFAULT NULL COMMENT 'MD5加密后的密码',
  `type` varchar(1) DEFAULT '2' COMMENT '教师为1，同学为2',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户';
