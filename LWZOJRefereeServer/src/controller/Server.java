package controller;

import java.net.*;
import java.sql.SQLException;
import java.util.List;
import java.io.*;

import dao.impl.CodeRepositoryDaoJdbcImpl;
import dao.CodeRepositoryDAO;
import dao.PointRepositoryDAO;
import dao.impl.PointRepositoryDaoJdbcImpl;
import dao.ScoreRepositoryDAO;
import dao.impl.ScoreRepositoryDaoJdbcImpl;
import model.Judge;
import model.Repetition;
import util.ConnectPool;

import org.apache.log4j.Logger;

/**
* 本类为服务器，对于客户端的请求进行响应，响应的方式为String
* @author 林智凯
* @version 1.1
*/
public class Server extends Thread
{
   private ServerSocket serverSocket;
   /**数据库连接池*/
   private static ConnectPool pool;
   
   public Server(int port) throws IOException
   {
      serverSocket = new ServerSocket(port);
      serverSocket.setSoTimeout(1000000);
   }
 
   @Override
   /**
	   * 这个方法将覆盖Thread的run()方法，运行服务器的套接字，响应用户的请求
	   */
   public void run()
   {
	   Logger log = Logger.getLogger(Server.class);
      while(true)
      {
    	 String result = "";
         try
         {
        	log.info("等待远程连接，端口号为：" + serverSocket.getLocalPort() + "...");
            Socket server = serverSocket.accept();
            log.info("远程主机地址：" + server.getRemoteSocketAddress());
            DataInputStream in = new DataInputStream(server.getInputStream());
            //接收客户端传入的数据，并写入日志
            String resultDecode = new String(in.readUTF());
            log.info("从" + server.getRemoteSocketAddress() + "接收" + resultDecode);
            //分割明文，执行对应的操作
            String[] resultSet = resultDecode.split(" ");
            //根据操作码，执行不同的操作
            switch(resultSet[0]) {
            
            //编程题
            case "1":{
            	result = Server.judgProgram(resultDecode);
            	break;
            }
            
            //选择题
            case "2":{
            	result = Server.judgChoice(resultDecode);
            	break;
            }
            
          //判断题
            case "3":{
            	result = Server.judgJudgment(resultDecode);
            	break;
            }
            
            default :{
            	break;
            }
            }
            
            //将响应结果发回客户端并写入日志
            DataOutputStream out = new DataOutputStream(server.getOutputStream());
            log.info("向" + server.getRemoteSocketAddress() + "发送" + out);
            out.writeUTF(result);
            
            server.close();
         }
         
         catch(SocketTimeoutException s)
         {
        	 log.info("Socket timed out!");
            break;
         }catch(IOException e)
         {
        	 log.error(e);
            e.printStackTrace();
            break;
         } catch (NumberFormatException e) {
			// TODO Auto-generated catch block
        	 log.error(e);
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(e);
			e.printStackTrace();
		}
      }
   }
   
   /**
	   * 这个方法用于编程题判题
	   */
   public static String judgProgram(String resultDecode) throws SQLException {
	   
	   //分割明文，执行对应的操作
       String[] resultSet = resultDecode.split(" ");
       //用户名
       String username = resultSet[1];   
       //班级名
       String classId = resultSet[2];    
       //题目集ID
       Integer collectionId = Integer.valueOf(resultSet[3]);   
       //题目ID
       Integer questionId = Integer.valueOf(resultSet[4]);    
       //重组代码
       StringBuilder sb = new StringBuilder();
       Integer start = 5;
       for (int i = start; i < resultSet.length; i++) {
    	   sb.append(" " + resultSet[i]);
       }
       //题目代码
       String code = sb.toString();
       //获得判题的结果
       String codeResult = Judge.judgeProgram(questionId, code);
       Integer state = 0;
       String str = "答案正确";
       if(str.equals(codeResult)) {
    	   state = 1;
       }
       else {
    	   state = 0;
       }
       
       CodeRepositoryDAO codeRepository = new CodeRepositoryDaoJdbcImpl();
       //获得代码相似度
       List<String> codes = codeRepository.selectAllCode(questionId);
       String repetition = Repetition.getRepetition(code, codes);
       //判断题目是否已被提交过
       if(codeRepository.selectCode(username, questionId)) {  
    	   //提交过
    	   codeRepository.updateCode(username, questionId, code, state);
    	}
   		else {    
   		//未提交过
   			codeRepository.submitCode(username, questionId, code, state);
   		}
   	
   		//计算总分并提交
   		PointRepositoryDAO pointRepository = new PointRepositoryDaoJdbcImpl();
   		//获取题集所有的题目id
   		List<Integer> programmtNum = pointRepository.getProgrammtNum(collectionId);    
   		ScoreRepositoryDAO scoreRepository = new ScoreRepositoryDaoJdbcImpl();
   		//获取用户完成的题目
   		List<Integer> completeNum = scoreRepository.getProgramCompleteNum(programmtNum, username);    
   		Integer score = scoreRepository.getProgramScore(completeNum);    
   		//获取得分
   		if(scoreRepository.updateProgrammingScore(username, collectionId, classId, score)) {
   			return "提交成功" + codeResult + "，查重率：" + repetition;
   		}
   		else {
   			return  "提交失败" + codeResult + "，查重率：" + repetition;
   		}
   	}

   /**
	   * 这个方法用于选择题判题
	   */
   public static String judgChoice(String resultDecode) throws SQLException {
	   //分割明文，执行对应的操作
       String[] resultSet = resultDecode.split(" ");
       //用户名
       String username = resultSet[1];   
       //班级名
       String classId = resultSet[2];    
       //题目集ID
       Integer collectionId = Integer.valueOf(resultSet[3]);   
       //作答答案字符串，用逗号分隔
       String[] userAnswer = resultSet[4].split(",");    
       PointRepositoryDAO pointRepository = new PointRepositoryDaoJdbcImpl();
       //获取选择题题号
       List<Integer> choiceNum = pointRepository.getChoiceNum(collectionId);
       //获取题号对应的答案
       List<String> rightAnswer = pointRepository.getChoiceAnswer(choiceNum);
       //统计回答正确的题目
       int count = 0;    
       for (int i = 0; i < userAnswer.length; i++) {
    	   if(userAnswer[i].equals(rightAnswer.get(i))) {
    		   count++;
    		   }
    	   }
       ScoreRepositoryDAO scoreRepository = new ScoreRepositoryDaoJdbcImpl();
       //获取题目总分
       int total = scoreRepository.getChoiceScore(collectionId);
       //算出得分
       Integer score = count * total / choiceNum.size();   
       //提交成功
       if(scoreRepository.updateChoiceScore(username, collectionId, classId, score)) {
    	   return "提交成功";
    	   }
       else{
    	   return "提交失败";
    	   }
   }
   
   /**
	   * 这个方法用于判断题判题
	   */
   public static String judgJudgment(String resultDecode) throws SQLException {
	   
	   //分割明文，执行对应的操作
       String[] resultSet = resultDecode.split(" ");
       //用户名
       String username = resultSet[1];   
       //班级名
       String classId = resultSet[2];    
       //题目集ID
       Integer collectionId = Integer.valueOf(resultSet[3]);   
       //作答答案字符串，用逗号分隔
       String[] userAnswer = resultSet[4].split(",");    
       PointRepositoryDAO pointRepository = new PointRepositoryDaoJdbcImpl();
       //获取判断题题号
       List<Integer> judgmentNum = pointRepository.getJudgmentNum(collectionId);
       //获取题号对应的答案
       List<String> rightAnswer = pointRepository.getJudgmentAnswer(judgmentNum);
       //统计回答正确的题目
       int count = 0;    
       for (int i = 0; i < userAnswer.length; i++) {
			if(userAnswer[i].equals(rightAnswer.get(i))) {
				count++;
			}
		}
       ScoreRepositoryDAO scoreRepository = new ScoreRepositoryDaoJdbcImpl();
       //获取题目总分
       int total = scoreRepository.getJudgmentScore(collectionId);
       //算出得分
       Integer score = count * total / judgmentNum.size();   
       //提交成功
       if(scoreRepository.updateJudgmentScore(username, collectionId, classId, score)) {
    	   return "提交成功";
    	   }
       else{
    	   return "提交失败";
    	   }
   }
   
   /**
	   * 数据库连接池的访问器
	   * @throws SQLException 
	   */
   public static ConnectPool getPool() {
	   return pool;
   }

   /**
	   * 数据库连接池的修改器
	   * @throws SQLException 
	   */
   public static void setPool() throws SQLException {
	   Server.pool = new ConnectPool();
   }

   /**
	   * 启动套接字，接收客户端的信息
       * @throws SQLException 
	   */
   public static void main(String [] args) throws SQLException
   {
      int port = Integer.parseInt("12001");
      //初始化数据库连接池
      Server.setPool();
      try
      {
         Thread t = new Server(port);
         t.run();
      }catch(IOException e)
      {
         e.printStackTrace();
      }
   }
}