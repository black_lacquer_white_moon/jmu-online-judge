package model;

import java.sql.SQLException;
import java.util.List;
import dao.impl.PointRepositoryDaoJdbcImpl;
import dao.impl.CommandDaoWindowsImpl;

/**
* Judge 类负责判题，包含批改编程题、选择题和判断题的方法
* @author 林智凯
* @version 1.0
*/
public class Judge {

	/**
	   * 该方法用于生成一个名为“main”的cpp文件，并且用测试点进行测试，返回作答情况
	   * @param String code:需要被写入cpp文件的C语言代码
	   * @param Integer num:题目编号
	   * @return String:返回判题的结果
	   */
	public static String judgeProgram(Integer num, String code) throws SQLException {
		
		String result = "error";
		//将代码写入cpp文件
		if(Program.writeFile(code)) {
			//实例化CommandDAOWindowsImpl对象
			CommandDaoWindowsImpl command = new CommandDaoWindowsImpl();
			//编译C语言代码
			String compilerResult = command.compileProgram();
			//编译成功
			if("".equals(compilerResult)) {    
				//实例化PointRepositoryJDBCImpl对象
				PointRepositoryDaoJdbcImpl pointRepository = new PointRepositoryDaoJdbcImpl();
				//获取num题目的测试点
				List<Testpoint> points = pointRepository.getTestpoint(num);
				//只有输出的测试点
				if(points.size() == 1 && points.get(0).getInput() == null) {
					//答案正确
					if(command.runOutput().equals(points.get(0).getOutput())) {
						result = "答案正确";
					}
					else {
						result = "答案错误";
					}
				}
				//既有输入也有输出的多个测试点
				else {
					boolean flag = true;
					for (int i = 0; i < points.size(); i++) {
						//生成测试点文件
						Program.writeTestPoint("text", points.get(i).getInput());
						//获得测试点的输出
						String textResult = command.runTestPoint();
						//System.out.println(textResult + points.get(i).getOutput());
						//答案错误
						System.out.println(points.get(i).getOutput());
						System.out.println(textResult);
						if(!textResult.equals(points.get(i).getOutput())) {
							result = "答案错误";
							flag = false;
							break;
						}
					}
					//答案都正确
					if(flag) {
						result = "答案正确";
					}
				}
			}
			else {    //编译错误
				result = "编译错误";
			}
		}
		else {    //写入文件发送错误
			result = "error";
		}
		
		return result;
	}
}
