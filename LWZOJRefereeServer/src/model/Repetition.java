package model;

import java.util.List;

/**
* Repetition 类用于完成代码查重
* @author 林智凯
* @version 1.0
*/
public class Repetition {
	
	/**
	   * 该方法用于计算被查重代码和代码样本之间的最大相似度
	   * @param code1 被查重代码，String
	   * @param codes 代码样本，List<String>
	   * @return String：代码相似度(单位0.01)
	   */
	public static String getRepetition(String code1, List<String> codes) {
		
		Integer maxRepetition = -1;
		//找到最相似的代码，计算出相似度
		for (String code : codes) {
			Integer repetition = Repetition.getLcs(code1, code);
			if(repetition > maxRepetition) {
				maxRepetition = repetition;
			}
		}
		Integer repetition = maxRepetition * 100 / code1.length();
		return repetition.toString() + "%";
	}
	
	/**
	   * 该方法用于计算2个字符串的最长公共子序列(LCS)的长度
	   * @param code1 被查重代码，String
	   * @param code2 代码样本，String
	   * @return Integer：2个字符创的LCS长度
	   */
	public static Integer getLcs(String code1, String code2) {
    	
    	Integer n = code1.length();
    	Integer m = code2.length();
    	//初始化动态规划矩阵
        int[][] dp = new int[n + 1][m + 1];
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= m; j++) {
                dp[i][j] = 0;
            }
        }
        //实现动态规划转换方程
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (code1.charAt(i - 1) == code2.charAt(j - 1)) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else if(dp[i - 1][j] > dp[i][j - 1]){
                    dp[i][j] =  dp[i - 1][j];   
                }
                else {
                	dp[i][j] = dp[i][j - 1];
                }
            }
        }
        return dp[n][m];
    }
	
}
