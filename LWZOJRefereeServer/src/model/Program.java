package model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
* Program 类负责将字符串写入文件中，包括cpp和text
* @author 林智凯
* @version 1.0
*/
public class Program {
	
	public static String CODE_PATH = System.getProperty("user.dir");
	
	 /**
	   * 该方法用于生成一个名为“main”的cpp文件
	   * @param String code:需要被写入cpp文件的C语言代码
	   * @return boolean：写入成功返回true，失败返回false
	   */
	public static boolean writeFile(String code)  {
		 try {
			 BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(CODE_PATH + "main.cpp", false)));
	         out.write(code);
	         out.close();
	         //System.out.println("文件创建成功！");
	         return true;
	         } catch (IOException e) {
	        	 e.printStackTrace();
	        	 return false;
	         }
	}
	
	/**
	   * 该方法用于生成一个名为filename的txt文件，作为测试点的参数
	   * @param String filename:文件名
	   * @param String text:需要被写入txt文件的字符串
	   * @return boolean：写入成功返回true，失败返回false
	   */
	public static boolean writeTestPoint(String filename, String text)  {
		 try {
			 BufferedWriter out = new BufferedWriter(new FileWriter(CODE_PATH + filename + ".txt"));
	         out.write(text);
	         out.close();
	         //System.out.println("文件创建成功！");
	         return true;
	         } catch (IOException e) {
	        	 e.printStackTrace();
	        	 return false;
	         }
	}
	
	/**
	   * 读取文件，测试用
	   */
	public static String readTxtFile(String filePath){
		 StringBuilder sb = new StringBuilder();   
		 try {
	        	
	                String encoding="UTF-8";
	                File file=new File(filePath);
	              //判断文件是否存在
	                if(file.isFile() && file.exists()){ 
	                	//考虑到编码格式
	                	InputStreamReader read = new InputStreamReader(
	                    new FileInputStream(file),encoding);
	                    BufferedReader bufferedReader = new BufferedReader(read);
	                    String lineTxt = null;
	                    while((lineTxt = bufferedReader.readLine()) != null){
	                        sb.append(lineTxt + "\n");
	                    }
	                    read.close();
	        }else{
	            System.out.println("找不到指定的文件");
	            return null;
	        }
	        } catch (Exception e) {
	            System.out.println("读取文件内容出错");
	            e.printStackTrace();
	        }
	        return sb.toString();
	    }
}
