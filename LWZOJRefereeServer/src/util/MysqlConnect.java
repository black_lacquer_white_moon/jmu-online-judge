package util;

import java.sql.*;

/**
* MysqlConnect类用于与数据库进行连接
* @author 林智凯
* @version 1.0
*/
public class MysqlConnect {
	
	/**连接数据库的URL*/
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
    static final String DB_URL = "jdbc:mysql://127.0.0.1:3306/lwzoj?characterEncoding=utf-8&useSSL=false";
    
    /**数据库的用户名与密码*/
    static final String USER = "root";
    static final String PASS = "root";
    
    /**
	   * 这个方法用于连接数据库
	   * @return 返回实例化的连接对象Connection
	   */
	public static Connection connectDatabase() throws SQLException{
		Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
		return conn;
	}
	
	/**
	   * 这个方法关闭连接对象(对Connection对象重载)
	   * @param conn 用于数据库连接的Connection对象
	   * @return 关闭资源，无返回值
	   */
	public static void close(Connection conn) {
		//如果conn连接对象不为空
		if(conn != null) {				
			try {
				//关闭conn连接对象对象
				conn.close();			
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	   * 这个方法关闭预处理对象(对PreparedStatement对象重载)
	   * @param pstmt 用于数据库连接的PreparedStatement对象
	   * @return 关闭资源，无返回值
	   */
	public static void close(PreparedStatement pstmt) {
		//如果pstmt预处理对象不为空
		if(pstmt != null) {				
			try {
				//关闭pstmt预处理对象
				pstmt.close();			
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	   * 这个方法关闭结果集对象(对ResultSet对象重载)
	   * @param rs 用于数据库连接的ResultSet对象
	   * @return 关闭资源，无返回值
	   */
	public static void close(ResultSet rs) {
		//如果rs结果集对象不为null
		if(rs != null) {				
			try {
				//关闭rs结果集对象
				rs.close();				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	   * 这个方法关闭预处理对象(对Statement对象重载)
	   * @param stmt 用于数据库连接的Statement对象
	   * @return 关闭资源，无返回值
	   */
	public static void close(Statement stmt) {
		//如果stmt预处理对象不为空
		if(stmt != null) {				
			try {
				//关闭stmt预处理对象
				stmt.close();			
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}