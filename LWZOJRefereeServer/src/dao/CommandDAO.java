package dao;

/**
* CommandDAO 接口指定了需要使用cmd命令完成的动作
* @author 林智凯
* @version 1.0
*/
public interface CommandDAO {
	
	/**
	   * 该方法用于将“main.cpp”的文件编译为可执行文件exe
	   * @return String：返回命令行回显信息，编译成功返回null
	   */
	public static String compileProgram(){
		return null;
	}
	
	/**
	   * 该方法用于将可执行文件用测试点测试
	   * @return String：程序用测试点测试后返回的结果
	   */
	public static String runTestPoint(){
		return null;
	}
}
