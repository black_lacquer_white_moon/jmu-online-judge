package dao;

import java.sql.SQLException;
import java.util.List;

import model.Testpoint;

/**
* PointRepository 接口指定了从数据库获取测试点数据的方法
* @author 林智凯
* @version 1.0
*/
public interface PointRepositoryDAO {

	/**
	   * 该方法用于将从存储层返回对应题目的测试点
	   * @param num 题目编号，Integer
	   * @return LinkedList<Testpoint>：List集合，存储对应题目的所有测试点：
	   * @throws SQLException
	   */
	public List<Testpoint> getTestpoint(Integer num) throws SQLException;
	
	/**
	   * 该方法用于将从存储层返回对应题目集包含的所有选择题题号
	   * @param num 题目集编号，Integer
	   * @return List<Integer>：List集合，存储对应题目集包含的所有选择题号
	   * @throws SQLException
	   */
	public List<Integer> getChoiceNum(Integer num) throws SQLException;
	
	/**
	   * 该方法用于将从存储层返回对应题目集包含的所有判断题题号
	   * @param num 题目集编号，Integer
	   * @return List<Integer>：List集合，存储对应题目集包含的所有判断题号
	   * @throws SQLException
	   */
	public List<Integer> getJudgmentNum(Integer num) throws SQLException;
	
	/**
	   * 该方法用于将从存储层返回对应题目集包含的所有编程题题号
	   * @param num 题目集编号，Integer
	   * @return List<Integer>：List集合，存储对应题目集包含的所有编程题号
	   * @throws SQLException
	   */
	public List<Integer> getProgrammtNum(Integer num) throws SQLException;
	
	/**
	   * 该方法用于将从存储层返回对应选择题的答案
	   * @param choiceNum List<Integer>题目集包含的所有选择题题号
	   * @return List<String>：List集合，存储对应题号对应的答案
	   * @throws SQLException
	   */
	public List<String> getChoiceAnswer(List<Integer> choiceNum) throws SQLException;
	
	/**
	   * 该方法用于将从存储层返回对应判断题的答案
	   * @param judgmentNum List<Integer>题目集包含的所有选择题题号
	   * @return List<Integer>：List集合，存储对应题号对应的答案
	   * @throws SQLException
	   */
	public List<String> getJudgmentAnswer(List<Integer> judgmentNum) throws SQLException;
}
