package dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import dao.CodeRepositoryDAO;
import util.MysqlConnect;
import controller.Server;

/**
* CodeRepositoryJDBCImpl 类基于MySQL数据库实现CodeRepositoryDAO接口
* @author 林智凯
* @version 1.0
*/
public class CodeRepositoryDaoJdbcImpl implements CodeRepositoryDAO{

	@Override
	/**
	   * 该方法用于将向存储层返回对应题目的测试点
	   * @param username 提交用户，String
	   * @param id 题目编号，Integer
	   * @param code 代码，String
	   * @return boolean：提交成功返回true，失败返回false
	   */
	public boolean submitCode(String username, Integer id, String code, Integer state) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;    
		//创建 ResultSet 结果集对象
		ResultSet rs = null;	
		boolean flag = false;
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();  
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlInsert = " INSERT INTO codes(username, questiond_id, code, state) values('%s', %d,'%s',%d); ";
			//判断插入是否成功
			if(statement.executeUpdate(String.format(sqlInsert, username, id, code, state)) != 0) {
				flag = true;
			}
			else {
				flag = false;
			}
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
		return flag;
	}

	@Override
	/**
	   * 该方法用于将在存储层查找题目是否提提交过
	   * @param username 提交用户，String
	   * @param id 题目编号，Integer
	   * @return boolean：已存在返回true，不存在返回false
	   */
	public boolean selectCode(String username, Integer id) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;   
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;   
		//创建 ResultSet 结果集对象
		ResultSet rs = null;	
		boolean flag = false;
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();   
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlSelect = "SELECT * FROM codes WHERE binary username = '%s' AND questiond_id = %d;";
			//查询是否曾经提交过了
			rs = statement.executeQuery(String.format(sqlSelect, username, id));
			rs.last();
			if(rs.getRow() > 0) {
				 //曾经提交过
				flag = true;   
			}
			else {
				//没提交过
			    flag = false;    
			}
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
		
		return flag;
	}

	@Override
	/**
	   * 该方法用于将在存储层更新某位同学某道题的代码
	   * @param username 提交用户，String
	   * @param id 题目编号，Integer
	   * @param code 代码，String
	   * @return boolean：更新成功返回true，更新失败返回false
	   */
	public boolean updateCode(String username, Integer id, String code, Integer state) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		 //创建静态 SQL 语句 Statement 对象
		Statement statement = null;   
		boolean flag = false;
		
		try {
			//数据库连接
			//conn = Server.getPool().getConnection();
			conn = MysqlConnect.connectDatabase();    
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlUpdate = "UPDATE codes SET code = '%s', state = %d WHERE username = '%s' AND questiond_id = %d;";
			//判断是否能更新成功
			if(statement.executeUpdate(String.format(sqlUpdate, code, state, username , id)) != 0) {
				flag = true;
			}
			else { 
				flag = false;
			}
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
		
		return flag;
	}

	@Override
	/**
	   * 该方法用于返回数据库中对应题目的所有代码样本
	   * @param id 题目编号，Integer
	   * @return List<String>：存储所有代码的List集合
	   * @throws SQLException
	   */
	public List<String> selectAllCode(Integer id) throws SQLException{
		
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;   
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;   
		//创建 ResultSet 结果集对象
		ResultSet rs = null;	
		
		List<String> codes = new LinkedList<String>();
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();   
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlSelect = "SELECT code FROM codes WHERE binary questiond_id = %d;";
			rs = statement.executeQuery(String.format(sqlSelect, id));
			//查询所有代码样本
			while(rs.next())
	        {
				String aCode = rs.getString("code");
				codes.add(aCode);
	        }
			return codes;
					
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
	}
}
