package dao.impl;

import java.io.*;

import model.Program;
import dao.CommandDAO;

/**
* CommandDAO 接口基于Window系统的实现类
* @author 林智凯
* @version 1.0
*/
public class CommandDaoWindowsImpl implements CommandDAO{
    
	public static String CODE_PATH = "D:\\";
	
	/**
	   * 该方法用于将“main.cpp”的文件编译为可执行文件exe
	   * @return String：返回命令行回显信息，编译成功返回null
	   */
	public String compileProgram(){
		BufferedReader br = null;
        StringBuilder outPutResult = new StringBuilder();
        try{
        	ProcessBuilder pb = new ProcessBuilder().command("cmd.exe", "/c", "gcc -o " + CODE_PATH + "main " + CODE_PATH + "main.cpp");
        	//外部程序的输出放到了错误信息输出流中
        	pb.redirectErrorStream(true);
        	//等待语句执行完成，否则可能会读不到结果。
        	Process process = pb.start();
        	process.waitFor();
        	InputStream inputStream = process.getInputStream();
        	br = new BufferedReader(new InputStreamReader(inputStream, "GBK"));
        	String line;
        	while ((line = br.readLine()) != null) {
        		outPutResult.append(line).append("\n");
        	}
        	br.close();
        	br = null;
        	//System.out.println(outPutResult.toString().equals(""));
        	return outPutResult.toString();
        	
        } catch (Exception e) {
        	e.printStackTrace();
        	return "error";
        }
	}
	
	/**
	   * 该方法用于将可执行文件用测试点测试
	   * @return String：程序用测试点测试后返回的结果
	   */
	public String runTestPoint(){
		BufferedReader br = null;
        StringBuilder outPutResult = new StringBuilder();
        try{
        	ProcessBuilder pb = new ProcessBuilder().command("cmd.exe", "/c", CODE_PATH + "main.exe < " + CODE_PATH + "text.txt");
        	pb.redirectErrorStream(true);
        	Process process = pb.start();
        	process.waitFor();
        	InputStream inputStream = process.getInputStream();
        	br = new BufferedReader(new InputStreamReader(inputStream, "GBK"));
        	String line;
        	while ((line = br.readLine()) != null) {
        		outPutResult.append(line).append("\n");
        	}
        	br.close();
        	br = null;
        } catch (Exception e) {
        	e.printStackTrace();
        	return null;
        }
		return outPutResult.toString();
	}

	/**
	   * 该方法用于将测试可执行文件的输出，供无输入的程序判题用
	   * @return String：程序用测试点测试后返回的结果
	   */
	public String runOutput(){
		BufferedReader br = null;
      StringBuilder outPutResult = new StringBuilder();
      try{
      	ProcessBuilder pb = new ProcessBuilder().command("cmd.exe", "/c", CODE_PATH + "main.exe");
      	pb.redirectErrorStream(true);
      	Process process = pb.start();
      	process.waitFor();
      	InputStream inputStream = process.getInputStream();
      	br = new BufferedReader(new InputStreamReader(inputStream, "GBK"));
      	String line;
      	while ((line = br.readLine()) != null) {
      		outPutResult.append(line).append("\n");
      	}
      	br.close();
      	br = null;
      } catch (Exception e) {
      	e.printStackTrace();
      	return null;
      }
		return outPutResult.toString();
	}
	
	/**
	   * 修改文件创建的路径
	   * @param String cODE_PATH：文件路径
	   */
	public static void setCodePath(String codePath) {
		CODE_PATH = codePath;
	}

	public static void main(String[] args) throws IOException    
	 {
		CommandDaoWindowsImpl command = new CommandDaoWindowsImpl();
		 String filePath = "D:\\a.txt";
	     String code = Program.readTxtFile(filePath);
	     Program.writeFile(code);
	     Program.writeTestPoint("text","10\n");
	     System.out.println(command.compileProgram());
	     System.out.println(command.runTestPoint());
	 }
	
}