package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import controller.Server;
/**
* ScoreRepositoryDAO 接口基于MySQL数据库的实现类
* @author 林智凯
* @version 1.0
*/
import dao.ScoreRepositoryDAO;
import util.MysqlConnect;

public class ScoreRepositoryDaoJdbcImpl implements ScoreRepositoryDAO {

	@Override
	/**
	   * 该方法根据用户的信息，更新题目集的选择题分数
	   * @param username String 用户名
	   * @param collection_id Integer 题目集id
	   * @param class_id Integer 班级 id
	   * @return boolean true为更新成功，false为更新失败
	   */
	public boolean updateChoiceScore(String username, Integer collectionId, String classId, Integer grade) throws SQLException {
		// TODO Auto-generated method stub
		 //创建 Connection 数据库连接对象
		Connection conn = null;   
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;    
		boolean flag = false;
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();   
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlUpdate = "UPDATE score SET choice = %d WHERE username = '%s' AND collection_id = %d AND class_id = '%s';";
			//判断是否能更新成功
			if(statement.executeUpdate(String.format(sqlUpdate, grade, username, collectionId, classId)) != 0) {
				flag = true;
			}
			else { 
				flag = false;
			}
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
		
		return flag;
	}

	@Override
	/**
	   * 该方法根据用户的信息，更新题目集的判断题题分数
	   * @param username String 用户名
	   * @param collection_id Integer 题目集id
	   * @param class_id Integer 班级 id
	   * @return boolean true为更新成功，false为更新失败
	   */
	public boolean updateJudgmentScore(String username, Integer collectionId, String classId, Integer grade) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		Statement statement = null;    
		//创建静态 SQL 语句 Statement 对象
		boolean flag = false;
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();    
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlUpdate = "UPDATE score SET judgment = %d WHERE username = '%s' AND collection_id = %d AND class_id = '%s';";
			//判断是否能更新成功
			if(statement.executeUpdate(String.format(sqlUpdate, grade, username, collectionId, classId)) != 0) {
				flag = true;
			}
			else { 
				flag = false;
			}
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
		
		return flag;
	}

	@Override
	/**
	   * 该方法根据用户的信息，更新题目集的编程题题分数
	   * @param username String 用户名
	   * @param collection_id Integer 题目集id
	   * @param class_id Integer 班级 id
	   * @return boolean true为更新成功，false为更新失败
	   */
	public boolean updateProgrammingScore(String username, Integer collectionId, String classId, Integer grade)
			throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;    
		boolean flag = false;
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();   
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlUpdate = "UPDATE score SET programming = %d WHERE username = '%s' AND collection_id = %d AND class_id = '%s';";
			//判断是否能更新成功
			if(statement.executeUpdate(String.format(sqlUpdate, grade, username, collectionId, classId)) != 0) {
				flag = true;
			}
			else { 
				flag = false;
			}
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    //关闭所有资源
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
		
		return flag;
	}

	@Override
	/**
	   * 该方法实现搜索对应题目集的选择题总分
	   * @param collection_id Integer 题目集id
	   * @return Integer 题目集选择题总分
	   */
	public Integer getChoiceScore(Integer collectionId) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;    
		//创建 ResultSet 结果集对象
		ResultSet rs = null;	
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();   
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlSelect = "SELECT score_choice FROM collections WHERE id = %d;";
			rs = statement.executeQuery(String.format(sqlSelect, collectionId));
			
			Integer socre = null;
			while(rs.next())
	        {
				socre = rs.getInt("score_choice");
	        }
			return socre;
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
	}

	@Override
	/**
	   * 该方法实现搜索对应题目集的判断题总分
	   * @param collection_id Integer 题目集id
	   * @return Integer 题目集判断题总分
	   */
	public Integer getJudgmentScore(Integer collectionId) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;    
		//创建 ResultSet 结果集对象
		ResultSet rs = null;	
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();   
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlSelect = "SELECT score_judgment FROM collections WHERE id = %d;";
			rs = statement.executeQuery(String.format(sqlSelect, collectionId));
			
			Integer socre = null;
			while(rs.next())
	        {
				socre = rs.getInt("score_judgment");
	        }
			return socre;
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
	}

	@Override
	/**
	   * 该方法获取对应用户完成的编程题号
	   * @param question_id List<Integer> 题目id
	   * @param username String 用户名
	   * @return Integer 题目集判断题总分
	   */
	public List<Integer> getProgramCompleteNum(List<Integer> questionId, String username) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建 ResultSet 结果集对象
		ResultSet rs = null;    
		//预编译的SQL语句对象
		PreparedStatement pStatement = null;    
		String sqlSelect = "SELECT questiond_id From codes WHERE username = ? AND questiond_id = ? AND state = 1;"; 
		
		try {
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();   
			pStatement = conn.prepareStatement(sqlSelect);
			//要返回的对象集
			List<Integer> programCompleteNum = new LinkedList<Integer>();    
			//把所有题号对应的答案搜出来，存进一个List
			for (Integer i : questionId) {
				//设置查找的用户
				pStatement.setString(1, username);    
				//设置要查找的题号
				pStatement.setInt(2, i);    
				
				rs = pStatement.executeQuery();
				//获取单个完成的题目题号
				while (rs.next()) {
					Integer id = rs.getInt("questiond_id");
					programCompleteNum.add(id);
				}
			}
			return programCompleteNum;
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(rs);
			MysqlConnect.close(pStatement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
		return null;
	}

	@Override
	/**
	   * 该方法获取用户答的题目的得分
	   * @param CompleteNum List<Integer> 已完成题目id
	   * @return Integer 用户答的题目的总分
	   */
	public Integer getProgramScore(List<Integer> completeNum) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;   
		//创建 ResultSet 结果集对象
		ResultSet rs = null;    
		//预编译的SQL语句对象
		PreparedStatement pStatement = null;    
		String sqlSelect = "SELECT score From program WHERE id = ?;"; 
		
		try {
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();   
			pStatement = conn.prepareStatement(sqlSelect);
			//要返回的分数
			Integer score = 0;    
			//把所有题号对应的答案搜出来算总分
			for (Integer i : completeNum) {
				//设置要查找的题号
				pStatement.setInt(1, i);    
				
				rs = pStatement.executeQuery();
				//计算得分
				while (rs.next()) {
					score += rs.getInt("score");
				}
			}
			return score;
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(rs);
			MysqlConnect.close(pStatement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
		return null;
	}

}
