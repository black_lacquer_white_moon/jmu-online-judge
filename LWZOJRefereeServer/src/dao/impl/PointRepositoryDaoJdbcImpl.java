package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import controller.Server;
import dao.PointRepositoryDAO;
import model.Testpoint;
import util.MysqlConnect;

/**
* PointRepository 接口基于MySQL数据库的实现类
* @author 林智凯
* @version 1.0
*/
public class PointRepositoryDaoJdbcImpl implements PointRepositoryDAO {

	@Override
	/**
	   * 基于 MySql 数据库实现的 getTestpoint 方法
	   * @param num 题目编号，Integer
	   * @return 测试点集合 List<Testpoint>
	 * @throws SQLException 
	   */
	public List<Testpoint> getTestpoint(Integer num) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;    
		//创建 ResultSet 结果集对象
		ResultSet rs = null;	
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();   
			 //初始化静态 SQL语句
			statement = conn.createStatement();   
			String sqlSelect = "SELECT * FROM testpoint WHERE binary question = '%s' ORDER BY id DESC;";
			rs = statement.executeQuery(String.format(sqlSelect, num.toString()));
			
			List<Testpoint> points = new LinkedList<Testpoint>();
			while(rs.next())
	        {
				//读取单行记录，转换为 Testpoint 对象
				Testpoint aPoint = new Testpoint(rs.getString("input"), rs.getString("output"));
				//加入收件箱
				points.add(aPoint);    
	        }
			return points;
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
	}

	@Override
	/**
	   * 该方法用于将从存储层返回对应题目集包含的所有选择题题号
	   * @param num 题目集编号，Integer
	   * @return List<Integer>：List集合，存储对应题目集包含的所有选择题题号
	   */
	public List<Integer> getChoiceNum(Integer num) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;  
		//创建 ResultSet 结果集对象 
		ResultSet rs = null;	
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();    
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlSelect = "SELECT choice FROM collections WHERE binary id = %d;";
			rs = statement.executeQuery(String.format(sqlSelect, num));
			//构造返回的题号List
			List<Integer> choiceNum = new LinkedList<Integer>();
			String[] choicenum = null;
			while(rs.next())
	        {
				//读取单行记录，按“,”分隔转换为 String 对象
				String str = rs.getString("choice");
				choicenum = str.split(",");
				
	        }
			//将字符串转换为Integer题号
			for (String str : choicenum) {
				choiceNum.add(Integer.valueOf(str));
			}
			choiceNum.sort(null);
			return choiceNum;
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
	}

	@Override
	/**
	   * 该方法用于将从存储层返回对应题目集包含的所有选择题题号
	   * @param num 题目集编号，Integer
	   * @return List<Integer>：List集合，存储对应题目集包含的所有选择题题号
	   */
	public List<Integer> getJudgmentNum(Integer num) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;    
		//创建 ResultSet 结果集对象
		ResultSet rs = null;	
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();    
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlSelect = "SELECT judgment FROM collections WHERE binary id = %d;";
			rs = statement.executeQuery(String.format(sqlSelect, num));
			//构造返回的题号List
			List<Integer> judgmentNum = new LinkedList<Integer>();
			String[] judgmentnum = null;
			while(rs.next())
	        {
				//读取单行记录，按“,”分隔转换为 String 对象
				String str = rs.getString("judgment");
				judgmentnum = str.split(",");
				
	        }
			//将字符串转换为Integer题号
			for (String str : judgmentnum) {
				judgmentNum.add(Integer.valueOf(str));
			}
			
			judgmentNum.sort(null);
			return judgmentNum;
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
	}

	@Override
	/**
	   * 该方法用于将从存储层返回对应选择题的答案
	   * @param choiceNum List<Integer>题目集包含的所有选择题题号
	   * @return List<String>：List集合，存储对应题号对应的答案
	   */
	public List<String> getChoiceAnswer(List<Integer> choiceNum) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;  
		//创建 ResultSet 结果集对象
		ResultSet rs = null;    
		//预编译的SQL语句对象
		PreparedStatement pStatement = null;    
		String sqlSelect = "SELECT answer From choice WHERE id = ?;"; 
		
		try {
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();  
			pStatement = conn.prepareStatement(sqlSelect);
			//要返回的对象集
			List<String> choiceAnswer = new LinkedList<String>();    
			//把所有题号对应的答案搜出来，存进一个List
			for (Integer i : choiceNum) {
				//设置要查找的题号
				pStatement.setInt(1, i);    
				rs = pStatement.executeQuery();
				//获取单个题目答案
				while (rs.next()) {
					String str = rs.getString("answer");
					choiceAnswer.add(str);
				}
			}
			return choiceAnswer;
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(rs);
			MysqlConnect.close(pStatement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
		return null;
	}

	@Override
	/**
	   * 该方法用于将从存储层返回对应判断题的答案
	   * @param judgmentNum List<Integer>题目集包含的所有选择题题号
	   * @return List<Integer>：List集合，存储对应题号对应的答案
	   */
	public List<String> getJudgmentAnswer(List<Integer> judgmentNum) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;   
		//创建 ResultSet 结果集对象
		ResultSet rs = null;    
		//预编译的SQL语句对象
		PreparedStatement pStatement = null;    
		String sqlSelect = "SELECT answer From judgment WHERE id = ?;"; 
		
		try {
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();  
			pStatement = conn.prepareStatement(sqlSelect);
			//要返回的对象集
			List<String> judgmentAnswer = new LinkedList<String>();    
			//把所有题号对应的答案搜出来，存进一个List
			for (Integer i : judgmentNum) {
				//设置要查找的题号
				pStatement.setInt(1, i);    
				rs = pStatement.executeQuery();
				//获取单个题目答案
				while (rs.next()) {
					String str = rs.getString("answer");
					judgmentAnswer.add(str);
				}
			}
			return judgmentAnswer;
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlConnect.close(rs);
			MysqlConnect.close(pStatement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
		return null;
	}

	@Override
	/**
	   * 该方法用于将从存储层返回对应题目集包含的所有编程题题号
	   * @param num 题目集编号，Integer
	   * @return List<Integer>：List集合，存储对应题目集包含的所有编程题号
	   */
	public List<Integer> getProgrammtNum(Integer num) throws SQLException {
		// TODO Auto-generated method stub
		//创建 Connection 数据库连接对象
		Connection conn = null;    
		//创建静态 SQL 语句 Statement 对象
		Statement statement = null;
		//创建 ResultSet 结果集对象
		ResultSet rs = null;	
		
		try {
			//数据库连接
			conn = Server.getPool().getConnection();
			//conn = MysqlConnect.connectDatabase();  
			//初始化静态 SQL语句
			statement = conn.createStatement();    
			String sqlSelect = "SELECT programming FROM collections WHERE binary id = %d;";
			rs = statement.executeQuery(String.format(sqlSelect, num));
			//构造返回的题号List
			List<Integer> programmtNum = new LinkedList<Integer>();
			String[] programmtNumber = null;
			while(rs.next())
	        {
				//读取单行记录，按“,”分隔转换为 String 对象
				String str = rs.getString("programming");
				programmtNumber = str.split(",");
				
	        }
			//将字符串转换为Integer题号
			for (String str : programmtNumber) {
				programmtNum.add(Integer.valueOf(str));
			}
			
			programmtNum.sort(null);
			return programmtNum;
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    
			//关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			Server.getPool().recoveryConnection(conn);
			//MysqlConnect.close(conn);
		}
	}

}
