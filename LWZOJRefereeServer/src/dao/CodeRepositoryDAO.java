package dao;

import java.sql.SQLException;
import java.util.List;

/**
* CodeRepository 接口指定了向存储层提交代码的方法
* @author 林智凯
* @version 1.0
*/
public interface CodeRepositoryDAO {

	/**
	   * 该方法用于将向存储层提交某位同学的某道题的代码
	   * @param username 提交用户，String
	   * @param id 题目编号，Integer
	   * @param code 代码，String
	   * @param state 解题状态
	   * @return boolean：提交成功返回true，失败返回false
	   * @throws SQLException
	   */
	public boolean submitCode(String username, Integer id , String code, Integer state) throws SQLException;
	
	/**
	   * 该方法用于将在存储层查找题目是否提提交过
	   * @param username 提交用户，String
	   * @param id 题目编号，Integer
	   * @param state 解题状态
	   * @return boolean：已存在返回true，不存在返回false
	   * @throws SQLException
	   */
	public boolean selectCode(String username, Integer id) throws SQLException;
	
	/**
	   * 该方法用于将向存储层更新某位同学的某道题的代码
	   * @param username 提交用户，String
	   * @param id 题目编号，Integer
	   * @param code 代码，String
	   * @param state 解题状态
	   * @return boolean：提交成功返回true，失败返回false
	   * @throws SQLException
	   */
	public boolean updateCode(String username, Integer id , String code, Integer state) throws SQLException;

	/**
	   * 该方法用于返回数据库中对应题目的所有代码样本
	   * @param id 题目编号，Integer
	   * @return List<String>：存储所有代码的List集合
	   * @throws SQLException
	   */
	public List<String> selectAllCode(Integer id) throws SQLException;
}
