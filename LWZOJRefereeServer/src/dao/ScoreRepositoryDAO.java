package dao;

import java.sql.SQLException;
import java.util.List;

/**
* ScoreRepositoryDAO 接口指定了修改存储层中同学分数的方法
* @author 林智凯
* @version 1.0
*/
public interface ScoreRepositoryDAO {
	
	/**
	   * 该方法根据用户的信息，更新题目集的选择题分数
	   * @param username String 用户名
	   * @param collectionId Integer 题目集id
	   * @param classId String 班级名
	   * @param grade Integer 
	   * @return boolean true为更新成功，false为更新失败
	   * @throws SQLException
	   */
	public boolean updateChoiceScore(String username, Integer collectionId, String classId, Integer grade) throws SQLException;
	
	/**
	   * 该方法根据用户的信息，更新题目集的判断题题分数
	   * @param username String 用户名
	   * @param collectionId Integer 题目集id
	   * @param classId String 班级名
	   * @param grade Integer 成绩
	   * @return boolean true为更新成功，false为更新失败
	   * @throws SQLException
	   */
	public boolean updateJudgmentScore(String username, Integer collectionId, String classId, Integer grade) throws SQLException;
	
	/**
	   * 该方法根据用户的信息，更新题目集的编程题题分数
	   * @param username String 用户名
	   * @param collectionId Integer 题目集id
	   * @param classId String 班级名
	   * @param grade Integer 成绩
	   * @return boolean true为更新成功，false为更新失败
	   * @throws SQLException
	   */
	public boolean updateProgrammingScore(String username, Integer collectionId, String classId, Integer grade) throws SQLException;
	
	/**
	   * 该方法实现搜索对应题目集的选择题总分
	   * @param collectionId Integer 题目集id
	   * @return Integer 题目集选择题总分
	   * @throws SQLException
	   */
	public Integer getChoiceScore(Integer collectionId) throws SQLException;
	
	/**
	   * 该方法实现搜索对应题目集的判断题总分
	   * @param collectionId Integer 题目集id
	   * @return Integer 题目集判断题总分
	   * @throws SQLException
	   */
	public Integer getJudgmentScore(Integer collectionId) throws SQLException;
	
	/**
	   * 该方法获取对应用户完成的编程题号
	   * @param questionId List<Integer> 题目id
	   * @param username String 用户名
	   * @return Integer 题目集判断题总分
	   * @throws SQLException
	   */
	public List<Integer> getProgramCompleteNum(List<Integer> questionId, String username) throws SQLException;
	
	/**
	   * 该方法获取用户答的题目的得分
	   * @param completeNum List<Integer> 已完成题目id
	   * @return Integer 用户答的题目的总分
	   * @throws SQLException
	   */
	public Integer getProgramScore(List<Integer> completeNum) throws SQLException;
}
